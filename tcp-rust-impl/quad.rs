use std::net::IpV4Addr;

#![derive(Debug, Hash, Eq, PartialEq, Clone, Copy)]
struct Quad {
    src: (IpV4Addr, u16),
    dst: (IpV4Addr, u16),
}
