extern crate etherparse;
extern crate tun_tap;

mod tcp;

use std::io;
use std::collections::HashMap;

use std::net::Ipv4Addr;

#[derive(Debug, Hash, Eq, PartialEq, Clone, Copy)]
pub struct Quad {
    pub src: (Ipv4Addr, u16),
    pub dst: (Ipv4Addr, u16),
}

fn main() -> io::Result<()> {
    let mut connections: HashMap<Quad, tcp::Connection> = Default::default();

    let mut nic = tun_tap::Iface::new("tun0", tun_tap::Mode::Tun)?;
    let mut buffer = [0u8; 1504];
    loop {
        let nbytes = nic.recv(&mut buffer[..])?;
        let ethernet_protocol = u16::from_be_bytes([buffer[2], buffer[3]]);

        if ethernet_protocol != 0x0800 {
            // we only handle IPv4 for now
            continue;
        }

        match etherparse::Ipv4HeaderSlice::from_slice(&buffer[4..nbytes]) {
            Ok(ip_header) => {
                if ip_header.protocol() != 0x06 {
                    // not TCP
                    continue;
                }

                match etherparse::TcpHeaderSlice::from_slice(&buffer[4+ip_header.slice().len()..nbytes]) {
                    Ok(tcp_header) => {
                        use std::collections::hash_map::Entry;
                        let data_length = 4 + ip_header.slice().len() + tcp_header.slice().len();
                        match connections.entry(Quad {
                            src: (ip_header.source_addr(), tcp_header.source_port()),
                            dst: (ip_header.destination_addr(), tcp_header.destination_port()),
                        }) {
                            Entry::Occupied(mut conn) => {
                                conn
                                    .get_mut()
                                    .on_packet(
                                        &mut nic,
                                        ip_header,
                                        tcp_header,
                                        &buffer[data_length..nbytes],
                                    )?;
                            },
                            Entry::Vacant(mut entry) => {
                                if let Some(conn) = tcp::Connection::accept(
                                    &mut nic,
                                    ip_header,
                                    tcp_header,
                                    &buffer[data_length..nbytes],
                                )? {
                                    entry.insert(conn);
                                }
                            },
                        }
                    },
                    Err(error) => eprintln!("Ignoring TCP packet with misformed header: {:?}", error),
                }
            },
            Err(error) => eprintln!("Ignoring misformed IPv4 packet: {:?}", error),
        }
    }
}
