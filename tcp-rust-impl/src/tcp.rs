use std::io;

pub enum State {
    Closed,
    Listen,
    SynRcvd,
    // Estab,
}

pub struct Connection {
    state: State,
    send_sequence_space: SendSequenceSpace,
    receive_sequence_space: RecvSequenceSpace,
}

struct SendSequenceSpace {
    /// send unacknowledged
    una: u32,
    /// send next
    nxt: u32,
    /// send window
    wnd: u16,
    /// send urgent pointer
    up: bool,
    /// segment sequence number used for last window update
    wl1: usize,
    /// segment sequence number used for last window update
    wl2: usize,
    /// initial sequence number
    iss: u32,
}

struct RecvSequenceSpace {
    /// recieve next
    nxt: u32,
    /// receive window
    wnd: u16,
    /// receive urgent pointer
    up: bool,
    /// initial receive sequence number
    irs: u32,
}

impl Connection {
    pub fn accept<'a>(
        nic: &mut tun_tap::Iface,
        ip_header: etherparse::Ipv4HeaderSlice<'a>,
        tcp_header: etherparse::TcpHeaderSlice,
        data: &'a [u8],
    ) -> io::Result<Option<Self>> {
        eprintln!(
            "{}:{} -> {}:{}, {} bytes of TCP",
            ip_header.source_addr(),
            tcp_header.source_port(),
            ip_header.destination_addr(),
            tcp_header.destination_port(),
            data.len(),
        );

        let mut buffer = [0u8; 1500];

        // only a SYN packet can start a connection
        if !tcp_header.syn() {
            return Ok(None);
        }

        // TODO: generate a random iss
        let iss = 0;
        let mut conn = Connection {
            state: State::SynRcvd,
            send_sequence_space: SendSequenceSpace {
                iss,
                una: iss,
                nxt: iss + 1,
                wnd: 10,
                up: false,
                wl1: 0,
                wl2: 0,
            },
            receive_sequence_space: RecvSequenceSpace {
                irs: tcp_header.sequence_number(),
                nxt: tcp_header.sequence_number() + 1,
                wnd: tcp_header.window_size(),
                up: false,
            },
        };

        let mut syn_ack = etherparse::TcpHeader::new(
            tcp_header.destination_port(),
            tcp_header.source_port(),
            conn.send_sequence_space.iss,
            conn.send_sequence_space.wnd,
        );
        syn_ack.acknowledgment_number = conn.receive_sequence_space.nxt;
        syn_ack.syn = true;
        syn_ack.ack = true;
        let mut ip = etherparse::Ipv4Header::new(
            syn_ack.header_len(),
            64,
            etherparse::IpTrafficClass::Tcp,
            [
                ip_header.destination()[0],
                ip_header.destination()[1],
                ip_header.destination()[2],
                ip_header.destination()[3],
            ],
            [
                ip_header.source()[0],
                ip_header.source()[1],
                ip_header.source()[2],
                ip_header.source()[3],
            ],
        );

        let mut unwritten = {
            let unwritten = &mut buffer[..];
            ip.write(&mut unwritten);
            syn_ack.write(&mut unwritten);
            unwritten.len();
        };
        nic.send(&buffer[..unwritten])?;
        Ok(Some(conn))
    }

    pub fn on_packet<'a>(
        &mut self,
        nic: &mut tun_tap::Iface,
        iph: etherparse::Ipv4HeaderSlice<'a>,
        tcph: etherparse::TcpHeaderSlice<'a>,
        data: &'a [u8],
    ) -> io::Result<()> {
        unimplemented!();
    }
}

impl Default for State {
    fn default() -> Self {
        // State::Closed
        State::Listen
    }
}
