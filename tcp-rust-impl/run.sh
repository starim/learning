#!/usr/bin/env bash

set -euo pipefail

project_path=/home/brent/Projects/learning/tcp-rust-impl
executable_path="$project_path/target/release/tcp-rust-impl"

cd "$project_path"
cargo build --release
sudo setcap CAP_NET_ADMIN=eip "$executable_path"
"$executable_path" &
pid=$!
sudo ip addr add 192.168.0.1/24 dev tun0
sudo ip link set up dev tun0
trap "kill $pid" INT TERM
wait $pid
