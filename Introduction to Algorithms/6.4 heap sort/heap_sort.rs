use std::cmp::PartialOrd;
use std::env;
use std::iter::Iterator;
use std::vec::Vec;

fn unsorted_vector() -> Vec<i32> {
    env::args().skip(1).map(|num_string|
        num_string.parse::<i32>().ok().expect("Couldn't parse input string to an integer.")
    ).collect()
}

/// This heap takes its contents by reference. See "6.5 priority queue" for a heap that owns its
/// contents.
struct Heap<'a, T: 'a> where T: PartialOrd {
    pub tree: &'a mut [T],
    pub heap_size: usize,
}

fn heap_sort<T: PartialOrd>(sequence: &mut [T]) {
    let size = sequence.len();
    let mut max_heap = build_max_heap(sequence);
    for n in (1..size).rev() {
        max_heap.tree.swap(0, n);
        max_heap.heap_size -= 1;
        maintain_max_heap(&mut max_heap, 0);
    }
}

fn build_max_heap<'a, T>(sequence: &'a mut [T]) -> Heap<'a, T> where T: PartialOrd {
    let size = sequence.len();
    let mut new_heap = Heap {
        tree: sequence,
        heap_size: size,
    };

    for index in (0..new_heap.heap_size/2).rev() {
        maintain_max_heap(&mut new_heap, index);
    }

    new_heap
}

fn maintain_max_heap<'a, T>(heap: &mut Heap<'a, T>, index: usize) where T: PartialOrd {
    let left_index = left_child_index(index);
    let right_index = right_child_index(index);
    let mut index_of_largest = index;
    if left_index < heap.heap_size && heap.tree[left_index] > heap.tree[index] {
        index_of_largest = left_index;
    }
    if right_index < heap.heap_size && heap.tree[right_index] > heap.tree[index_of_largest] {
        index_of_largest = right_index;
    }
    if index_of_largest != index {
        heap.tree.swap(index, index_of_largest);
        maintain_max_heap(heap, index_of_largest);
    }
}

#[allow(dead_code)]
fn parent_index(index: usize) -> usize {
    (index + 1)/2 - 1
}

fn left_child_index(index: usize) -> usize {
    (2 * (index + 1)) - 1
}

fn right_child_index(index: usize) -> usize {
    2 * (index + 1)
}

fn main() {
    let mut sequence = unsorted_vector();
    // the list's size won't change past this point, so optimize memory by freeing reserved
    // capacity in the vector
    sequence.shrink_to_fit();
    heap_sort(&mut sequence);
    let printable_sequence = sequence.iter().map(|&number| number.to_string()).collect::<Vec<String>>();
    println!("{}", printable_sequence.join(" "));
}

#[test]
fn test_empty_sequence() {
    let mut sequence :Vec<i32> = vec![];
    let expected_output :Vec<i32> = vec![];

    heap_sort(&mut sequence);
    assert_eq!(sequence, expected_output);
}
#[test]
fn test_one_element_sequence() {
    let mut sequence = vec![5];
    let expected_output = vec![5];

    heap_sort(&mut sequence);
    assert_eq!(sequence, expected_output);
}
#[test]
fn test_many_element_sequence() {
    let mut sequence = vec![-8, 923, 17, 1, 15, -72, -23849, 0, 129];
    let expected_output = vec![-23849, -72, -8, 0, 1, 15, 17, 129, 923];

    heap_sort(&mut sequence);
    assert_eq!(sequence, expected_output);
}
#[test]
fn test_already_sorted_sequence() {
    let mut sequence = vec![-72, -8, 0, 1, 15, 17, 129, 923];
    let expected_output = vec![-72, -8, 0, 1, 15, 17, 129, 923];

    heap_sort(&mut sequence);
    assert_eq!(sequence, expected_output);
}
#[test]
fn test_sequence_with_duplicates() {
    let mut sequence = vec![-8, 923, 17, 1, 17, -72, -72, 0, 129];
    let expected_output = vec![-72, -72, -8, 0, 1, 17, 17, 129, 923];

    heap_sort(&mut sequence);
    assert_eq!(sequence, expected_output);
}
