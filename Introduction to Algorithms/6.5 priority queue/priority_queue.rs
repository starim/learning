use std::vec::Vec;

mod heap;
use heap::OwnedHeap;

struct PriorityQueue {
    heap: OwnedHeap<f32>,
}

impl PriorityQueue {
    pub fn new(sequence: Vec<f32>) -> PriorityQueue {
        PriorityQueue {
            heap: OwnedHeap::new_max_heap(sequence),
        }
    }

    pub fn maximum(&self) -> Option<f32> {
        if self.heap.heap_size > 0 {
            self.heap.tree.get(0).map(|&value| value)
        } else {
            None
        }
    }

    pub fn pop_maximum(&mut self) -> Option<f32> {
        let maximum = self.maximum();
        if maximum.is_some() {
            self.heap.tree.swap(0, self.heap.heap_size - 1);
            self.heap.heap_size -= 1;
            self.heap.maintain_max_heap(0);
        }

        return maximum;
    }

    pub fn increase_key(&mut self, mut index: usize, new_value: f32) {
        assert!(index < self.heap.heap_size, "Index larger than heap");
        assert!(new_value >= self.heap.tree[index], "New value cannot be less than old value.");
        self.heap.tree[index] = new_value;
        while
            index > 0 &&
            self.heap.tree[heap::parent_index(index)] < self.heap.tree[index]
        {
            self.heap.tree.swap(index, heap::parent_index(index));
            index = heap::parent_index(index);
        }
    }

    pub fn insert(&mut self, value: f32) {
        // the backing Vec might not have enough capacity
        if self.heap.tree.len() == self.heap.heap_size {
            self.heap.tree.push(0.0);
        }

        self.heap.heap_size += 1;
        let new_index = self.heap.heap_size - 1;
        self.heap.tree[new_index] = std::f32::NEG_INFINITY;
        self.increase_key(new_index, value);
    }
}

#[test]
fn maximum_for_empty_queue() {
    let priority_queue = PriorityQueue::new(vec![]);
    assert_eq!(priority_queue.maximum(), None);
}
#[test]
fn maximum_for_one_element_queue() {
    let priority_queue = PriorityQueue::new(vec![5.0]);
    assert_eq!(priority_queue.maximum(), Some(5.0));
}
#[test]
fn maximum_for_many_element_queue() {
    let priority_queue = PriorityQueue::new(
        vec![-8.8, 92.3, 1.7, 1.0, 1.5, -7.2, -23.849, 0.0, 12.9]
    );
    assert_eq!(priority_queue.maximum(), Some(92.3));
}
#[test]
fn maximum_for_queue_with_many_elements_with_duplicates() {
    let priority_queue = PriorityQueue::new(
        vec![-8.8, 92.3, 1.7, 1.0, 1.7, -7.2, -7.2, 0.0, 92.3]
    );
    assert_eq!(priority_queue.maximum(), Some(92.3));
}

#[test]
fn pop_maximum_for_empty_queue() {
    let mut priority_queue = PriorityQueue::new(vec![]);
    assert_eq!(priority_queue.pop_maximum(), None);
    assert_eq!(priority_queue.pop_maximum(), None);
}
#[test]
fn pop_maximum_for_one_element_queue() {
    let mut priority_queue = PriorityQueue::new(vec![5.0]);
    assert_eq!(priority_queue.pop_maximum(), Some(5.0));
    assert_eq!(priority_queue.pop_maximum(), None);
}
#[test]
fn pop_maximum_for_many_element_queue() {
    let mut priority_queue = PriorityQueue::new(
        vec![-8.8, 92.3, 1.7, 1.0, 1.5, -7.2, -23.849, 0.0, 12.9]
    );
    assert_eq!(priority_queue.pop_maximum(), Some(92.3));
    assert_eq!(priority_queue.pop_maximum(), Some(12.9));
    assert_eq!(priority_queue.pop_maximum(), Some(1.7));
    assert_eq!(priority_queue.pop_maximum(), Some(1.5));
    assert_eq!(priority_queue.pop_maximum(), Some(1.0));
    assert_eq!(priority_queue.pop_maximum(), Some(0.0));
    assert_eq!(priority_queue.pop_maximum(), Some(-7.2));
    assert_eq!(priority_queue.pop_maximum(), Some(-8.8));
    assert_eq!(priority_queue.pop_maximum(), Some(-23.849));
    assert_eq!(priority_queue.pop_maximum(), None);
}
#[test]
fn pop_maximum_for_queue_with_many_elements_with_duplicates() {
    let mut priority_queue = PriorityQueue::new(
        vec![1.7, 92.3, 1.7, 0.0, 1.7, -7.2, -7.2, 0.0, 92.3]
    );
    assert_eq!(priority_queue.pop_maximum(), Some(92.3));
    assert_eq!(priority_queue.pop_maximum(), Some(92.3));
    assert_eq!(priority_queue.pop_maximum(), Some(1.7));
    assert_eq!(priority_queue.pop_maximum(), Some(1.7));
    assert_eq!(priority_queue.pop_maximum(), Some(1.7));
    assert_eq!(priority_queue.pop_maximum(), Some(0.0));
    assert_eq!(priority_queue.pop_maximum(), Some(0.0));
    assert_eq!(priority_queue.pop_maximum(), Some(-7.2));
    assert_eq!(priority_queue.pop_maximum(), Some(-7.2));
    assert_eq!(priority_queue.pop_maximum(), None);
}

#[test]
#[should_panic]
fn increase_key_for_nonexistent_key() {
    let mut priority_queue = PriorityQueue::new(vec![]);
    priority_queue.increase_key(0, 1.0)
}
#[test]
fn increase_key_for_existenting_key() {
    let mut priority_queue = PriorityQueue::new(vec![5.0]);
    priority_queue.increase_key(0, 6.5);
    assert_eq!(priority_queue.pop_maximum(), Some(6.5));
    assert_eq!(priority_queue.pop_maximum(), None);
}
#[test]
fn increase_key_for_many_element_queue() {
    let mut priority_queue = PriorityQueue::new(
        vec![-8.8, 92.3, 1.7, 1.0, 1.5, -7.2, -23.849, 0.0, 12.9]
    );
    priority_queue.increase_key(3, 1.6);
    assert_eq!(priority_queue.pop_maximum(), Some(92.3));
    assert_eq!(priority_queue.pop_maximum(), Some(12.9));
    assert_eq!(priority_queue.pop_maximum(), Some(1.7));
    assert_eq!(priority_queue.pop_maximum(), Some(1.6));
    assert_eq!(priority_queue.pop_maximum(), Some(1.5));
    assert_eq!(priority_queue.pop_maximum(), Some(0.0));
    assert_eq!(priority_queue.pop_maximum(), Some(-7.2));
    assert_eq!(priority_queue.pop_maximum(), Some(-8.8));
    assert_eq!(priority_queue.pop_maximum(), Some(-23.849));
    assert_eq!(priority_queue.pop_maximum(), None);
}
#[test]
fn increase_key_for_queue_with_many_elements_with_duplicates() {
    let mut priority_queue = PriorityQueue::new(
        vec![1.7, 92.3, 1.7, 0.0, 1.7, -7.2, -7.2, 0.0, 92.3]
    );
    priority_queue.increase_key(5, 1.7);
    assert_eq!(priority_queue.pop_maximum(), Some(92.3));
    assert_eq!(priority_queue.pop_maximum(), Some(92.3));
    assert_eq!(priority_queue.pop_maximum(), Some(1.7));
    assert_eq!(priority_queue.pop_maximum(), Some(1.7));
    assert_eq!(priority_queue.pop_maximum(), Some(1.7));
    assert_eq!(priority_queue.pop_maximum(), Some(1.7));
    assert_eq!(priority_queue.pop_maximum(), Some(0.0));
    assert_eq!(priority_queue.pop_maximum(), Some(0.0));
    assert_eq!(priority_queue.pop_maximum(), Some(-7.2));
    assert_eq!(priority_queue.pop_maximum(), None);
}
#[test]
#[should_panic]
fn increase_key_panics_on_negative_increment() {
    let mut priority_queue = PriorityQueue::new(vec![5.0]);
    priority_queue.increase_key(0, -1.5);
}

#[test]
fn insert_into_empty_queue() {
    let mut priority_queue = PriorityQueue::new(vec![]);
    priority_queue.insert(-2.4);
    assert_eq!(priority_queue.maximum(), Some(-2.4));
}
#[test]
fn insert_into_one_element_queue() {
    let mut priority_queue = PriorityQueue::new(vec![5.0]);
    priority_queue.insert(5.1);
    assert_eq!(priority_queue.maximum(), Some(5.1));
}
#[test]
fn insert_unique_value_into_many_element_queue() {
    let mut priority_queue = PriorityQueue::new(
        vec![-8.8, 92.3, 1.7, 1.0, 1.5, -7.2, -23.849, 0.0, 12.9]
    );
    priority_queue.insert(1.8);
    assert_eq!(priority_queue.pop_maximum(), Some(92.3));
    assert_eq!(priority_queue.pop_maximum(), Some(12.9));
    assert_eq!(priority_queue.pop_maximum(), Some(1.8));
    assert_eq!(priority_queue.pop_maximum(), Some(1.7));
    assert_eq!(priority_queue.pop_maximum(), Some(1.5));
    assert_eq!(priority_queue.pop_maximum(), Some(1.0));
    assert_eq!(priority_queue.pop_maximum(), Some(0.0));
    assert_eq!(priority_queue.pop_maximum(), Some(-7.2));
    assert_eq!(priority_queue.pop_maximum(), Some(-8.8));
    assert_eq!(priority_queue.pop_maximum(), Some(-23.849));
    assert_eq!(priority_queue.pop_maximum(), None);
}
#[test]
fn insert_duplicate_value_into_many_element_queue() {
    let mut priority_queue = PriorityQueue::new(
        vec![-8.8, 92.3, 1.7, 1.0, 1.5, -7.2, -23.849, 0.0, 12.9]
    );
    priority_queue.insert(-7.2);
    assert_eq!(priority_queue.pop_maximum(), Some(92.3));
    assert_eq!(priority_queue.pop_maximum(), Some(12.9));
    assert_eq!(priority_queue.pop_maximum(), Some(1.7));
    assert_eq!(priority_queue.pop_maximum(), Some(1.5));
    assert_eq!(priority_queue.pop_maximum(), Some(1.0));
    assert_eq!(priority_queue.pop_maximum(), Some(0.0));
    assert_eq!(priority_queue.pop_maximum(), Some(-7.2));
    assert_eq!(priority_queue.pop_maximum(), Some(-7.2));
    assert_eq!(priority_queue.pop_maximum(), Some(-8.8));
    assert_eq!(priority_queue.pop_maximum(), Some(-23.849));
    assert_eq!(priority_queue.pop_maximum(), None);
}
#[test]
fn insert_duplicate_value_into_full_queue() {
    let mut priority_queue = PriorityQueue::new(
        vec![-8.8, 92.3, 1.7, 1.0, 1.5, -7.2, -23.849, 0.0, 12.9]
    );
    priority_queue.insert(-7.2);
    assert_eq!(priority_queue.pop_maximum(), Some(92.3));
    assert_eq!(priority_queue.pop_maximum(), Some(12.9));
    assert_eq!(priority_queue.pop_maximum(), Some(1.7));
    assert_eq!(priority_queue.pop_maximum(), Some(1.5));
    assert_eq!(priority_queue.pop_maximum(), Some(1.0));
    assert_eq!(priority_queue.pop_maximum(), Some(0.0));
    assert_eq!(priority_queue.pop_maximum(), Some(-7.2));
    assert_eq!(priority_queue.pop_maximum(), Some(-7.2));
    assert_eq!(priority_queue.pop_maximum(), Some(-8.8));
    assert_eq!(priority_queue.pop_maximum(), Some(-23.849));
    assert_eq!(priority_queue.pop_maximum(), None);
}
