/// This heap takes ownership of its contents. See "6.4 heap sort" for a heap that managers
/// references to its contents.
pub struct OwnedHeap<T> where T: PartialOrd {
    pub tree: Vec<T>,
    pub heap_size: usize,
}

impl<T> OwnedHeap<T> where T: PartialOrd {
    pub fn new_max_heap(sequence: Vec<T>) -> OwnedHeap<T> {
        let size = sequence.len();
        let mut new_heap = OwnedHeap {
            tree: sequence,
            heap_size: size,
        };

        for index in (0..new_heap.heap_size/2).rev() {
            new_heap.maintain_max_heap(index);
        }

        new_heap
    }

    pub fn maintain_max_heap(&mut self, index: usize) {
        let left_index = left_child_index(index);
        let right_index = right_child_index(index);
        let mut index_of_largest = index;
        if left_index < self.heap_size && self.tree[left_index] > self.tree[index] {
            index_of_largest = left_index;
        }
        if right_index < self.heap_size && self.tree[right_index] > self.tree[index_of_largest] {
            index_of_largest = right_index;
        }
        if index_of_largest != index {
            self.tree.swap(index, index_of_largest);
            self.maintain_max_heap(index_of_largest);
        }
    }
}

#[allow(dead_code)]
pub fn parent_index(index: usize) -> usize {
    (index + 1)/2 - 1
}

#[allow(dead_code)]
pub fn left_child_index(index: usize) -> usize {
    (2 * (index + 1)) - 1
}

#[allow(dead_code)]
pub fn right_child_index(index: usize) -> usize {
    2 * (index + 1)
}
