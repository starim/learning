#!/usr/bin/env bash

# Compares time to complete insertion sort using Rust in release mode, Rust in
# debug mode, and Ruby.

set -euo pipefail

n=150000

# set the working directory to the script directory so we can use relative
# paths
cd "$(dirname "$0")"

if command -v time > /dev/null; then
	echo "time utility found, moving on to testing..."
else
	echo "This script requires the time utility be installed and available on the PATH"
	exit 1
fi

echo "Compiling Rust programs..."

rustc -C opt-level=3 -o /tmp/insertion_sort_release '2.1 insertion sort/rust/insertion_sort.rs'
rustc -C opt-level=3 -o /tmp/bubble_sort_release 'bubble sort/bubble_sort.rs'
rustc -C opt-level=3 -o /tmp/selection_sort_release 'selection sort/selection_sort.rs'
rustc -C opt-level=3 -o /tmp/merge_sort_release 'merge sort/merge_sort.rs'
rustc -C opt-level=3 -o /tmp/heap_sort_release '6.4 heap sort/heap_sort.rs'
rustc -C opt-level=3 -o /tmp/quicksort_release '7.1 quicksort/quicksort.rs'
rustc -C opt-level=3 -o /tmp/counting_sort_release '8.2 counting sort/counting_sort.rs'

# copy the Ruby program to /tmp so it benefits from running from a ramdisk like the Rust programs do
cp '2.1 insertion sort/ruby/insertion_sort.rb' /tmp/ruby_insertion_sort.rb

echo "Building input sequence..."
cd gen_random_sequence
cargo build --release
input=$(cargo run --release -- $n)

format='elapsed time: %E  CPU usage: %P  Mem usage: %MKb'

rust_insertion_sort="/tmp/insertion_sort_release $input"
rust_bubble_sort="/tmp/bubble_sort_release $input"
rust_selection_sort="/tmp/selection_sort_release $input"
rust_merge_sort="/tmp/merge_sort_release $input"
rust_heap_sort="/tmp/heap_sort_release $input"
rust_quicksort="/tmp/quicksort_release $input"
rust_counting_sort="/tmp/counting_sort_release 9999 $input"
ruby_insertion_sort="/tmp/ruby_insertion_sort.rb $input"

echo "Time to sort $n elements:"
echo Rust insertion sort:
/usr/bin/time -f "$format" -- $rust_insertion_sort 1>/dev/null
echo Rust bubble sort:
/usr/bin/time -f "$format" -- $rust_bubble_sort 1>/dev/null
echo Rust selection sort:
/usr/bin/time -f "$format" -- $rust_selection_sort 1>/dev/null
echo Rust merge sort:
/usr/bin/time -f "$format" -- $rust_merge_sort 1>/dev/null
echo Rust heap sort:
/usr/bin/time -f "$format" -- $rust_heap_sort 1>/dev/null
echo Rust quicksort:
/usr/bin/time -f "$format" -- $rust_quicksort 1>/dev/null
echo Rust counting sort:
/usr/bin/time -f "$format" -- $rust_counting_sort 1>/dev/null
echo Ruby insertion sort:
/usr/bin/time -f "$format" -- $ruby_insertion_sort 1>/dev/null
