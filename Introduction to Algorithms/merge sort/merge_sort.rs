use std::cmp::PartialOrd;
use std::env;
use std::iter::Iterator;
use std::vec::Vec;

fn unsorted_vector() -> Vec<i32> {
    env::args().skip(1).map(|num_string|
        num_string.parse::<i32>().ok().expect("Couldn't parse input string to an integer.")
    ).collect()
}

fn merge_sort<T: PartialOrd + Clone>(sequence: &mut [T]) {
    let n = sequence.len();

    if n < 2 {
        return;
    }

    let mut sorted_sequence = Vec::with_capacity(n);
    {
        let (mut first_half, mut last_half) = sequence.split_at_mut(n/2);

        merge_sort(&mut first_half);
        merge_sort(&mut last_half);

        let mut first_half_iter = first_half.iter().cloned().peekable();
        let mut last_half_iter = last_half.iter().cloned().peekable();

        for _ in 0..n {
            if first_half_iter.peek().is_some() && last_half_iter.peek().is_some() {
                if first_half_iter.peek() < last_half_iter.peek() {
                    sorted_sequence.push(first_half_iter.next().unwrap());
                } else {
                    sorted_sequence.push(last_half_iter.next().unwrap());
                }
            } else if first_half_iter.peek().is_some() {
                sorted_sequence.push(first_half_iter.next().unwrap());
            } else {
                sorted_sequence.push(last_half_iter.next().unwrap());
            }
        }
    }

    for (index, value) in sorted_sequence.into_iter().enumerate() {
        sequence[index] = value;
    }
}

fn main() {
    let mut sequence = unsorted_vector();
    // the list's size won't change past this point, so optimize memory by freeing reserved
    // capacity in the vector
    sequence.shrink_to_fit();
    merge_sort(&mut sequence);
    let printable_sequence = sequence.iter().map(|&number| number.to_string()).collect::<Vec<String>>();
    println!("{}", printable_sequence.join(" "));
}

#[test]
fn test_empty_sequence() {
    let mut sequence :Vec<i32> = vec![];
    let expected_output :Vec<i32> = vec![];

    merge_sort(&mut sequence);
    assert_eq!(sequence, expected_output);
}
#[test]
fn test_one_element_sequence() {
    let mut sequence = vec![5];
    let expected_output = vec![5];

    merge_sort(&mut sequence);
    assert_eq!(sequence, expected_output);
}
#[test]
fn test_many_element_sequence() {
    let mut sequence = vec![-8, 923, 17, 1, 15, -72, -23849, 0, 129];
    let expected_output = vec![-23849, -72, -8, 0, 1, 15, 17, 129, 923];

    merge_sort(&mut sequence);
    assert_eq!(sequence, expected_output);
}
#[test]
fn test_already_sorted_sequence() {
    let mut sequence = vec![-72, -8, 0, 1, 15, 17, 129, 923];
    let expected_output = vec![-72, -8, 0, 1, 15, 17, 129, 923];

    merge_sort(&mut sequence);
    assert_eq!(sequence, expected_output);
}
