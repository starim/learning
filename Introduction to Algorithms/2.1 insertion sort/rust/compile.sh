#!/usr/bin/env bash

set -euo pipefail

# set the working directory to the script directory so we can use relative
# paths
cd "$(dirname "$0")"

rustc -C opt-level=3 -o insertion_sort_release insertion_sort.rs
rustc -g -o insertion_sort_debug insertion_sort.rs
