extern crate rand;

use rand::Rng;

/// Prints a sequence of n space-separated unsigned integers where n is the first argument to this
/// program. The numbers generated are in the range [0, 9,999].
fn main() {
    let args: Vec<String> = std::env::args().collect();

    if args.len() < 2 {
        println!("Length of sequence to generate required.");
        std::process::exit(1);
    }

    let length = args[1].parse::<usize>().expect("Cannot interpret first argument as an integer.");
    println!("{}", random_integer_sequence(length).join(" "));
}

fn random_integer_sequence(length: usize) -> Vec<String> {
    let mut rng = rand::thread_rng();
    let mut sequence = Vec::with_capacity(length);
    for _ in 0..length {
        sequence.push(rng.gen_range(0, 9_999).to_string());
    }

    sequence
}

#[test]
fn sequence_has_expected_length() {
    let sequence = random_integer_sequence(5);
    assert_eq!(sequence.len(), 5);
}
