use std::env;
use std::iter::Iterator;
use std::vec::Vec;

fn unsorted_vector() -> Vec<u32> {
    // this is modified from the standard for the sorting programs in order to allow passing in the
    // maxiumum input value
    env::args().skip(2).map(|num_string|
        num_string
            .parse::<u32>()
            .ok()
            .expect("Couldn't parse input string into an unsigned integer.")
    ).collect()
}

fn counting_sort(sequence: &[u32], max: u32) -> Vec<u32> {
    let mut num_items_less_than_or_equal = vec![0; (max + 1) as usize];
    for j in 0..sequence.len() {
        num_items_less_than_or_equal[sequence[j] as usize] += 1;
    }
    for i in 1..=max as usize {
        num_items_less_than_or_equal[i] += num_items_less_than_or_equal[i - 1];
    }

    let mut sorted = Vec::with_capacity(sequence.len());
    unsafe {
        // this is safe as long as sorted is allocated with sequence.len() capacity and it is only
        // written to
        sorted.set_len(sequence.len());
    }
    for k in (0..sequence.len()).rev() {
        sorted[num_items_less_than_or_equal[sequence[k] as usize] - 1 as usize] = sequence[k];
        num_items_less_than_or_equal[sequence[k] as usize] -= 1;
    }

    sorted
}

fn main() {
    let max =
        env::args()
        .skip(1)
        .next()
        .expect("First argument should be the maximum value for the unsorted sequence.")
        .parse::<u32>()
        .ok()
        .expect("Couldn't parse maximum value argument into an unsigned integer");
    let mut sequence = unsorted_vector();
    // the list's size won't change past this point, so optimize memory by freeing reserved
    // capacity in the vector
    sequence.shrink_to_fit();
    let sorted = counting_sort(&sequence, max);
    let printable_sequence = sorted.iter().map(|&number| number.to_string()).collect::<Vec<String>>();
    println!("{}", printable_sequence.join(" "));
}

#[test]
fn test_empty_sequence() {
    let sequence = vec![];
    let expected_output = vec![];

    let output = counting_sort(&sequence, 1_000);
    assert_eq!(output, expected_output);
}
#[test]
fn test_one_element_sequence() {
    let sequence = vec![5];
    let expected_output = vec![5];

    let output = counting_sort(&sequence, 1_000);
    assert_eq!(output, expected_output);
}
#[test]
fn test_many_element_sequence() {
    let sequence = vec![8, 923, 17, 1, 15, 72, 284, 0, 129];
    let expected_output = vec![0, 1, 8, 15, 17, 72, 129, 284, 923];

    let output = counting_sort(&sequence, 1_000);
    assert_eq!(output, expected_output);
}
#[test]
fn test_already_sorted_sequence() {
    let sequence = vec![72, 8, 0, 1, 15, 17, 129, 923];
    let expected_output = vec![0, 1, 8, 15, 17, 72, 129, 923];

    let output = counting_sort(&sequence, 1_000);
    assert_eq!(output, expected_output);
}
#[test]
fn test_sequence_with_duplicates() {
    let sequence = vec![8, 923, 17, 1, 17, 72, 72, 0, 129];
    let expected_output = vec![0, 1, 8, 17, 17, 72, 72, 129, 923];

    let output = counting_sort(&sequence, 1_000);
    assert_eq!(output, expected_output);
}
