use std::cmp::PartialOrd;
use std::env;
use std::iter::Iterator;
use std::vec::Vec;

fn unsorted_vector() -> Vec<i32> {
    env::args().skip(1).map(|num_string|
        num_string.parse::<i32>().ok().expect("Couldn't parse input string to an integer.")
    ).collect()
}

fn quicksort_start<T: PartialOrd + Clone>(sequence: &mut [T]) {
    if sequence.len() == 0 {
        return;
    }
    let last_index = sequence.len() - 1;
    quicksort(sequence, 0, last_index);
}

fn quicksort<T: PartialOrd + Clone>(sequence: &mut [T], slice_start: usize, slice_end: usize) {
    if slice_start >= slice_end {
        return;
    }

    let partition_index = partition(sequence, slice_start, slice_end);
    if partition_index > 0 {
        quicksort(sequence, slice_start, partition_index - 1);
    }
    quicksort(sequence, partition_index + 1, slice_end);
}

fn partition<T: PartialOrd + Clone>(sequence: &mut [T], slice_start: usize, slice_end: usize) -> usize {
    let pivot = sequence[slice_end].clone();
    let mut i = slice_start;
    for j in slice_start..slice_end {
        if sequence[j] <= pivot {
            sequence.swap(i, j);
            i = i + 1;
        }
    }
    sequence.swap(i, slice_end);
    return i;
}

fn main() {
    let mut sequence = unsorted_vector();
    // the list's size won't change past this point, so optimize memory by freeing reserved
    // capacity in the vector
    sequence.shrink_to_fit();
    quicksort_start(&mut sequence);
    let printable_sequence = sequence.iter().map(|&number| number.to_string()).collect::<Vec<String>>();
    println!("{}", printable_sequence.join(" "));
}

#[test]
fn test_empty_sequence() {
    let mut sequence :Vec<i32> = vec![];
    let expected_output :Vec<i32> = vec![];

    quicksort_start(&mut sequence);
    assert_eq!(sequence, expected_output);
}
#[test]
fn test_one_element_sequence() {
    let mut sequence = vec![5];
    let expected_output = vec![5];

    quicksort_start(&mut sequence);
    assert_eq!(sequence, expected_output);
}
#[test]
fn test_many_element_sequence() {
    let mut sequence = vec![-8, 923, 17, 1, 15, -72, -23849, 0, 129];
    let expected_output = vec![-23849, -72, -8, 0, 1, 15, 17, 129, 923];

    quicksort_start(&mut sequence);
    assert_eq!(sequence, expected_output);
}
#[test]
fn test_already_sorted_sequence() {
    let mut sequence = vec![-72, -8, 0, 1, 15, 17, 129, 923];
    let expected_output = vec![-72, -8, 0, 1, 15, 17, 129, 923];

    quicksort_start(&mut sequence);
    assert_eq!(sequence, expected_output);
}
#[test]
fn test_sequence_with_duplicates() {
    let mut sequence = vec![-8, 923, 17, 1, 17, -72, -72, 0, 129];
    let expected_output = vec![-72, -72, -8, 0, 1, 17, 17, 129, 923];

    quicksort_start(&mut sequence);
    assert_eq!(sequence, expected_output);
}
