mod dielectric;
mod lambertian;
mod metal;
pub use dielectric::Dielectric;
pub use lambertian::Lambertian;
pub use metal::Metal;

use nalgebra::Vector3;

use crate::hittable::Hit;
use crate::ray::Ray;

#[derive(Debug, Copy, Clone)]
pub struct Scattering {
    pub attenuation: Vector3<f64>,
    pub scattered_ray: Ray,
}

pub trait Material {
    fn scatter(&self, ray: Ray, hit: &Hit, rng: &mut crate::RngImpl) -> Option<Scattering>;
}
