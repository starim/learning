use nalgebra::Vector3;

use super::{Material, Scattering};
use crate::hittable::Hit;
use crate::ray::Ray;
use crate::util;

#[derive(Debug, Copy, Clone)]
pub struct Metal {
    albedo: Vector3<f64>,
    fuzz: f64,
}

impl Metal {
    pub fn new(albedo: Vector3<f64>, fuzz: f64) -> Self {
        Metal { albedo, fuzz: fuzz.min(1.0) }
    }
}

impl Material for Metal {
    fn scatter(&self, ray: Ray, hit: &Hit, rng: &mut crate::RngImpl) -> Option<Scattering> {
        let reflected = util::reflect(*ray.unit_direction().as_ref(), hit.normal);
        let scattered_direction = reflected + (self.fuzz * util::random_in_unit_sphere(rng));
        let scattered = Ray::new(hit.p, scattered_direction);
        if scattered.direction().dot(&hit.normal) > 0.0 {
            Some(Scattering {
                attenuation: self.albedo,
                scattered_ray: scattered,
            })
        } else {
            None
        }
    }
}
