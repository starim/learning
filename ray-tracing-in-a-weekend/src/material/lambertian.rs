use nalgebra::Vector3;

use super::{Material, Scattering};
use crate::hittable::Hit;
use crate::ray::Ray;
use crate::util;

#[derive(Debug, Copy, Clone)]
pub struct Lambertian {
    albedo: Vector3<f64>,
}

impl Lambertian {
    pub fn new(albedo: Vector3<f64>) -> Self {
        Lambertian { albedo }
    }
}

impl Material for Lambertian {
    fn scatter(&self, _ray: Ray, hit: &Hit, rng: &mut crate::RngImpl) -> Option<Scattering> {
        let target = hit.p + hit.normal + util::random_in_unit_sphere(rng);
        Some(Scattering {
            attenuation: self.albedo,
            scattered_ray: Ray::new(hit.p, target - hit.p),
        })
    }
}
