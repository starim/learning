use nalgebra::Vector3;
use rand::Rng;

use super::{Material, Scattering};
use crate::hittable::Hit;
use crate::ray::Ray;
use crate::util;

#[derive(Debug, Copy, Clone)]
pub struct Dielectric {
    ref_idx: f64,
}

impl Dielectric {
    pub fn new(ref_idx: f64) -> Self {
        Dielectric { ref_idx }
    }
}

impl Material for Dielectric {
    fn scatter(&self, ray: Ray, hit: &Hit, rng: &mut crate::RngImpl) -> Option<Scattering> {
        let reflected = util::reflect(ray.direction(), hit.normal);
        let (outward_normal, ni_over_nt, cosine) =
            if ray.direction().dot(&hit.normal) > 0.0 {
                (
                    -hit.normal,
                    self.ref_idx,
                    self.ref_idx * ray.direction().dot(&hit.normal)/ray.direction().magnitude(),
                )
            } else {
                (
                    hit.normal,
                    1.0/self.ref_idx,
                    -ray.direction().dot(&hit.normal)/ray.direction().magnitude()
                )
            };

        let scattered =
            if let Some(refracted) = util::refract(ray.direction(), outward_normal, ni_over_nt) {
                let reflect_prob = util::schlick(cosine, self.ref_idx);
                if rng.gen::<f64>() < reflect_prob {
                    Ray::new(hit.p, reflected)
                } else {
                    Ray::new(hit.p, refracted)
                }
            } else {
                Ray::new(hit.p, reflected)
            };

        Some(Scattering {
            attenuation: Vector3::<f64>::new(1.0, 1.0, 1.0),
            scattered_ray: scattered,
        })
    }
}
