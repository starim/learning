use nalgebra::{Unit, Vector3};
use rand::distributions::{Distribution, Uniform};

pub fn random_in_unit_sphere(rng: &mut crate::RngImpl) -> Vector3<f64> {
    let unit_cube = Uniform::from(-1.0f64..1.0f64);
    loop {
        let p = Vector3::<f64>::new(
            unit_cube.sample(rng),
            unit_cube.sample(rng),
            unit_cube.sample(rng),
        );
        if p.magnitude_squared() < 1.0 {
            return p;
        }
    }
}

pub fn random_in_unit_disk(rng: &mut crate::RngImpl) -> Vector3<f64> {
    let unit_cube = Uniform::from(-1.0f64..1.0f64);
    loop {
        let p = Vector3::<f64>::new(
            unit_cube.sample(rng),
            unit_cube.sample(rng),
            0.0,
        );
        if p.magnitude_squared() < 1.0 {
            return p;
        }
    }
}

pub fn reflect(incoming_vector: Vector3<f64>, surface_normal: Vector3<f64>) -> Vector3<f64> {
    incoming_vector - 2.0 * incoming_vector.dot(&surface_normal) * surface_normal
}

pub fn refract(v: Vector3<f64>, n: Vector3<f64>, ni_over_nt: f64) -> Option<Vector3<f64>> {
    let v_direction = Unit::new_normalize(v);
    let dt = v_direction.dot(&n);
    let discriminant = 1.0 - ni_over_nt * ni_over_nt * (1.0 - dt * dt);
    if discriminant > 0.0 {
        Some(ni_over_nt * (v_direction.as_ref() - n * dt) - n * discriminant.sqrt())
    } else {
        None
    }
}

pub fn schlick(cosine: f64, ref_idx: f64) -> f64 {
    let mut r0 = (1.0 - ref_idx)/(1.0 + ref_idx);
    r0 = r0 * r0;
    r0 + (1.0 - r0) * (1.0 - cosine).powi(5)
}
