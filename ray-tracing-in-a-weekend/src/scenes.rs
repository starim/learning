use nalgebra::Vector3;
use rand::distributions::{Distribution, Uniform};
use rand::Rng;

use crate::hittable::Hittable;
use crate::material::{Dielectric, Lambertian, Metal};
use crate::sphere::Sphere;

pub fn random_spheres_scene(rng: &mut crate::RngImpl) -> Vec<Box<dyn Hittable + Sync>> {
    let mut scene: Vec<Box<dyn Hittable + Sync>> = Vec::new();

    scene.push(Box::new(Sphere::new(
        Vector3::new(0.0, -1_000.0, 0.0),
        1_000.0,
        Box::new(Lambertian::new(Vector3::<f64>::new(0.5, 0.5, 0.5))),
    )));

    let zero_to_one = Uniform::from(0.0f64..1.0f64);
    let point_five_to_one = Uniform::from(0.5f64..1.0f64);
    let zero_to_point_five = Uniform::from(0.0f64..0.5f64);

    for a in -11..11 {
        for b in -11..11 {
            let choose_material = rng.gen::<f64>();
            let center = Vector3::<f64>::new(
                a as f64 + 0.9 * rng.gen::<f64>(),
                0.2,
                b as f64 + 0.9 * rng.gen::<f64>(),
            );

            if (center - Vector3::<f64>::new(4.0, 0.2, 0.0)).magnitude() > 0.9 {
                if choose_material < 0.8 {
                    // diffuse
                    scene.push(Box::new(Sphere::new(
                        center,
                        0.2, 
                        Box::new(Lambertian::new(Vector3::<f64>::new(
                            zero_to_one.sample(rng) * zero_to_one.sample(rng),
                            zero_to_one.sample(rng) * zero_to_one.sample(rng),
                            zero_to_one.sample(rng) * zero_to_one.sample(rng),
                        ))),
                    )));
                } else if choose_material < 0.95 {
                    // metal
                    scene.push(Box::new(Sphere::new(
                        center,
                        0.2, 
                        Box::new(Metal::new(
                            Vector3::<f64>::new(
                                point_five_to_one.sample(rng),
                                point_five_to_one.sample(rng),
                                point_five_to_one.sample(rng),
                            ),
                            zero_to_point_five.sample(rng),
                        )),
                    )));
                } else {
                    // glass
                    scene.push(Box::new(Sphere::new(
                        center,
                        0.2,
                        Box::new(Dielectric::new(1.5)),
                    )));
                }
            }
        }
    }

    scene.push(Box::new(Sphere::new(
        Vector3::<f64>::new(0.0, 1.0, 0.0),
        1.0,
        Box::new(Dielectric::new(1.5))
    )));
    scene.push(Box::new(Sphere::new(
        Vector3::<f64>::new(-4.0, 1.0, 0.0),
        1.0, 
        Box::new(Lambertian::new(Vector3::<f64>::new(0.4, 0.2, 0.1))),
    )));
    scene.push(Box::new(Sphere::new(
        Vector3::<f64>::new(4.0, 1.0, 0.0),
        1.0, 
        Box::new(Metal::new(Vector3::<f64>::new(0.7, 0.6, 0.5), 0.0)),
    )));

    scene
}
