use nalgebra::Vector3;

use crate::material::Material;
use crate::ray::Ray;

pub struct Hit<'a> {
    pub t: f64,
    pub p: Vector3<f64>,
    pub normal: Vector3<f64>,
    pub material: &'a (dyn Material + std::marker::Sync),
}

pub trait Hittable {
    fn hit(&self, ray: Ray, t_min: f64, t_max: f64) -> Option<Hit>;
}
