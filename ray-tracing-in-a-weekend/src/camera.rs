use std::f64::consts::PI;

use nalgebra::{Unit, Vector3};

use crate::ray::Ray;
use crate::util;

pub struct Camera {
    origin: Vector3<f64>,
    lower_left_corner: Vector3<f64>,
    horizontal: Vector3<f64>,
    vertical: Vector3<f64>,
    lens_radius: f64,

    // basis vectors
    u: Unit<Vector3<f64>>,
    v: Unit<Vector3<f64>>,
    w: Unit<Vector3<f64>>,
}

impl Camera {
    pub fn new(
        look_from: Vector3<f64>,
        look_at: Vector3<f64>,
        up: Vector3<f64>,
        vfov: f64,
        aspect: f64,
        aperture: f64,
        focus_distance: f64,
    ) -> Self {
        let theta = vfov * PI/180.0;
        let half_height = (theta/2.0).tan();
        let half_width = aspect * half_height;
        let w = Unit::new_normalize(look_from - look_at);
        let u = Unit::new_normalize(up.cross(w.as_ref()));
        let v = Unit::new_normalize(w.cross(u.as_ref()));

        Camera {
            lower_left_corner: look_from - half_width * focus_distance * u.as_ref() - half_height * focus_distance * v.as_ref() - focus_distance * w.as_ref(),
            horizontal: 2.0 * half_width * focus_distance * u.as_ref(),
            vertical: 2.0 * half_height * focus_distance * v.as_ref(),
            origin: look_from,
            lens_radius: aperture/2.0,
            u,
            v,
            w,
        }
    }

    pub fn outgoing_ray(&self, s: f64, t: f64, rng: &mut crate::RngImpl) -> Ray {
        let rd = self.lens_radius * util::random_in_unit_disk(rng);
        let offset = self.u.as_ref() * rd[0] + self.v.as_ref() * rd[2];
        Ray::new(
            self.origin + offset,
            self.lower_left_corner + s * self.horizontal + t * self.vertical - self.origin - offset,
        )
    }
}
