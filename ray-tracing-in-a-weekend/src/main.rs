extern crate image;
extern crate indicatif;
extern crate nalgebra;
extern crate rand;
extern crate rayon;
extern crate structopt;

mod camera;
mod hittable;
mod material;
mod ray;
mod scenes;
mod sphere;
mod util;

pub const T_MIN: f64 = 0.001;
pub const T_MAX: f64 = std::f64::MAX;
pub const MAX_RAY_REFLECTIONS: u32 = 50;
pub const CAMERA_POSITION_X: f64 = 13.0;
pub const CAMERA_POSITION_Y: f64 = 2.0;
pub const CAMERA_POSITION_Z: f64 = 3.0;
pub const CAMERA_LOOK_AT_X: f64 = 0.0;
pub const CAMERA_LOOK_AT_Y: f64 = 0.0;
pub const CAMERA_LOOK_AT_Z: f64 = 0.0;
pub const CAMERA_FIELD_OF_VIEW_DEGREES: f64 = 20.0;

use std::path::PathBuf;

use nalgebra::Vector3;
use rayon::prelude::*;
use structopt::StructOpt;
use rand::Rng;

use crate::camera::Camera;
use crate::hittable::{Hit, Hittable};
use crate::ray::Ray;

type RngImpl = rand::rngs::StdRng;

#[derive(StructOpt, Debug)]
#[structopt(name = "basic")]
struct Args {
    #[structopt(short = "s", long = "seed")]
    seed: Option<u64>,
    #[structopt(long = "width", default_value = "1600")]
    width: u32,
    #[structopt(long = "height", default_value = "800")]
    height: u32,
    #[structopt(long = "samples", default_value = "100")]
    num_antialias_samples: u32,
    #[structopt(short = "o", long = "output", parse(from_os_str), default_value = "output.png")]
    output_file_name: PathBuf,
    /// disable parallel execution
    #[structopt(long = "sequential")]
    run_sequential: bool,
}

fn main() {
    let args = Args::from_args();
    let seed = args.seed.unwrap_or_else(|| {
        let generated_seed = rand::random::<u64>();
        generated_seed
    });
    println!("Using seed {}", seed);
    let mut rng: RngImpl = rand::SeedableRng::seed_from_u64(seed);

    let mut image_buffer: Vec<u8> = Vec::with_capacity((args.width * args.height) as usize);
    let camera_position = Vector3::<f64>::new(CAMERA_POSITION_X, CAMERA_POSITION_Y, CAMERA_POSITION_Z);
    let camera_look_at = Vector3::<f64>::new(CAMERA_LOOK_AT_X, CAMERA_LOOK_AT_Y, CAMERA_LOOK_AT_Z);
    let camera = Camera::new(
        camera_position,
        camera_look_at,
        Vector3::<f64>::new(0.0, 1.0, 0.0),
        CAMERA_FIELD_OF_VIEW_DEGREES,
        (args.width as f64)/(args.height as f64),
        0.1,
        10.0,
    );

    let scene = scenes::random_spheres_scene(&mut rng);

    let progress_bar = indicatif::ProgressBar::new((args.height * args.width) as u64);
    progress_bar.set_style(
        indicatif::ProgressStyle::default_bar()
            .template("{wide_bar} [ETA: {eta}]")
    );

    for y in (0..args.height).rev() {
        for x in 0..args.width {

            let mut color = if args.num_antialias_samples == 0 {
                let u = (x as f64)/(args.width as f64);
                let v = (y as f64)/(args.height as f64);
                color(camera.outgoing_ray(u, v, &mut rng), &*scene, &mut rng, 0)
            } else {
                if args.run_sequential {
                    (0..args.num_antialias_samples).into_iter().map(|_| {
                        let u = (x as f64 + rand::random::<f64>())/(args.width as f64);
                        let v = (y as f64 + rand::random::<f64>())/(args.height as f64);
                        color(camera.outgoing_ray(u, v, &mut rng), &*scene, &mut rng, 0)
                    })
                    .sum::<Vector3<f64>>()/(args.num_antialias_samples as f64)
                } else {
                    (0..args.num_antialias_samples)
                        .map(|_| { rng.gen::<u64>() })
                        // having to collect() here is unfortunate but I couldn't find a way to get
                        // rayon to iterate over the output of the previous map()
                        .collect::<Vec<u64>>()
                        .into_par_iter()
                        .map(|seed| {
                            let mut rng: RngImpl = rand::SeedableRng::seed_from_u64(seed);
                            let u = (x as f64 + rand::random::<f64>())/(args.width as f64);
                            let v = (y as f64 + rand::random::<f64>())/(args.height as f64);
                            color(camera.outgoing_ray(u, v, &mut rng), &*scene, &mut rng, 0)
                        })
                        .sum::<Vector3<f64>>()/(args.num_antialias_samples as f64)
                }
            };

            color = gamma_correct(color);

            let r = (color.x * 255.0).round() as u8;
            let g = (color.y * 255.0).round() as u8;
            let b = (color.z * 255.0).round() as u8;
            image_buffer.extend_from_slice(&[r, g, b]);

            progress_bar.inc(1);
        }
    }

    save_image(
        &args.output_file_name.to_string_lossy(),
        args.width,
        args.height,
        &*image_buffer,
    );
}

fn color(ray: Ray, scene_objects: &[Box<dyn Hittable + Sync>], rng: &mut RngImpl, depth: u32) -> Vector3<f64> {
    // TODO: see if there's a more elegant iterator like min() to avoid the manual fold() logic
    let closest_hit_option: Option<Hit> =
        scene_objects
        .iter()
        .filter_map(|object| object.hit(ray, T_MIN, T_MAX))
        .fold(None, |closest_hit_so_far_option, this_hit| {
            match closest_hit_so_far_option {
                Some(closest_hit_so_far) => {
                    if closest_hit_so_far.t < this_hit.t {
                        Some(closest_hit_so_far)
                    } else {
                        Some(this_hit)
                    }
                },
                None => Some(this_hit),
            }
        });
    if let Some(hit) = closest_hit_option {
        if depth < MAX_RAY_REFLECTIONS {
            if let Some(scattering) = hit.material.scatter(ray, &hit, rng) {
                let color = color(scattering.scattered_ray, scene_objects, rng, depth + 1);
                return Vector3::<f64>::new(
                    scattering.attenuation.x * color.x,
                    scattering.attenuation.y * color.y,
                    scattering.attenuation.z * color.z,
                );
            }
        }

        Vector3::<f64>::zeros()
    } else {
        let unit_direction = ray.unit_direction();
        let t = 0.5 * (unit_direction.y + 1.0);
        Vector3::<f64>::new(1.0, 1.0, 1.0).lerp(&Vector3::<f64>::new(0.5, 0.7, 1.0), t)
    }
}

fn gamma_correct(input: Vector3<f64>) -> Vector3<f64> {
    Vector3::<f64>::new(input.x.sqrt(), input.y.sqrt(), input.z.sqrt())
}

fn save_image(filename: &str, width: u32, height: u32, buffer: &[u8]) {
    image::save_buffer(filename, buffer, width, height, image::RGB(8))
        .expect("Failed to save image.");
}
