use image;

let image = image::open("earth.png").expect("Can't find image").to_rgb();
let (nx, ny) = image.dimensions();
let pixels = image.into_raw();
let texture = ImageTexture::new(pixels, nx, ny);

world.add(Box::new(Sphere::new(
    Vec3::new(0.0, 2.0, 0.0),
    2.0,
    Arc::new(Lambertian::new(Box::new(texture))),
)));
