use nalgebra::Vector3;

use crate::hittable::{Hit, Hittable};
use crate::material::Material;
use crate::ray::Ray;

pub struct Sphere {
    center: Vector3<f64>,
    radius: f64,
    material: Box<dyn Material + std::marker::Sync>,
}

impl Sphere {
    pub fn new(center: Vector3<f64>, radius: f64, material: Box<dyn Material + Sync>) -> Self {
        Sphere { center, radius, material }
    }
}

impl Hittable for Sphere {
    fn hit(&self, ray: Ray, t_min: f64, t_max: f64) -> Option<Hit> {
        let oc = ray.origin() - self.center;
        let a = ray.direction().dot(&(ray.direction()));
        let b = oc.dot(&(ray.direction()));
        let c = oc.dot(&oc) - (self.radius * self.radius);
        let discriminant = (b * b) - (a * c);
        if discriminant > 0.0 {
            let t_hit = (-b - discriminant.sqrt())/a;
            if (t_min..t_max).contains(&t_hit) {
                return Some(Hit {
                    t: t_hit,
                    p: ray.point(t_hit),
                    normal: (ray.point(t_hit) - self.center)/self.radius,
                    material: &*self.material,
                });
            } else {
                let t_hit = (-b + discriminant.sqrt())/a;
                if (t_min..t_max).contains(&t_hit) {
                    return Some(Hit {
                        t: t_hit,
                        p: ray.point(t_hit),
                        normal: (ray.point(t_hit) - self.center)/self.radius,
                        material: &*self.material,
                    });
                }
            }
        }

        None
    }
}
