#![feature(vec_resize_default)]

use std::collections::hash_map::DefaultHasher;
use std::hash::{Hash, Hasher};
use std::mem;

const DEFAULT_INITIAL_CAPACITY: usize = 100;

/// The load factor threshold that the hash table will try to stay below by growing its backing
/// vector whenever the load factor exceeds this threshold. Java uses a threshold of 0.75 for its
/// HashMap type, so I borrowed that value.
const MAX_LOAD_FACTOR: f32 = 0.75;

/// a hash table implemented using open addressing via Robin Hood hashing
#[derive(Debug)]
pub struct HashTable<K, V> {
    // because the backing vector is never actually resized, using an ArrayVec would probably be
    // better
    table: Vec<Option<Bucket<K, V>>>,
    size: usize,
}

#[derive(Debug)]
struct Bucket<K, V> {
    hash: u64,
    key: K,
    value: V,
}

impl<K: Hash + Eq, V> HashTable<K, V> {

    pub fn new() -> Self {
        HashTable::new_with_capacity(DEFAULT_INITIAL_CAPACITY)
    }

    pub fn new_with_capacity(capacity: usize) -> Self {
        let mut table = Vec::with_capacity(0);
        table.resize_default(capacity);

        HashTable {
            table,
            size: 0,
        }
    }

    /// Inserts the given key, value pair into the hash table. May reallocate the backing vector if
    /// the load factor grows too large.
    pub fn insert<A: Into<K>, B: Into<V>>(&mut self, key: A, value: B) {
        self.insert_raw(key.into(), value.into());
        if self.load_factor() > MAX_LOAD_FACTOR {
            self.grow_storage();
        }
    }

    fn insert_raw(&mut self, key: K, value: V) {
        let capacity = self.table.len();
        let mut new_node = Bucket { hash: self.hash(&key), key, value };

        let mut new_node_distance = 0usize;
        loop {
            // TODO: wrap around instead of going off the end of the backing array--this will
            // complicate distance calculations because simple subtraction won't suffice anymore
            let bucket_being_probed =
                (new_node.hash as usize + new_node_distance) % self.table.len();

            match self.table[bucket_being_probed as usize] {
                // collision
                Some(ref mut existing_node) => {
                    if existing_node.key == new_node.key {
                        mem::replace(existing_node, new_node);
                        return;
                    } else {
                        let existing_node_distance = HashTable::<K, V>::displacement(
                            existing_node.hash as usize,
                            bucket_being_probed,
                            capacity,
                        );
                        if existing_node_distance < new_node_distance {
                            mem::swap(&mut new_node, existing_node);
                            new_node_distance = existing_node_distance;
                        }
                    }
                },
                // no collision
                None => {
                    self.table[bucket_being_probed as usize] = Some(new_node);
                    self.size += 1;
                    return;
                },
            }

            new_node_distance += 1;
        }
    }

    /// returns the value for the given key, or None if no value exists corresponding to the given
    /// key
    pub fn get<A: Into<K>>(&self, key: A) -> Option<&V> {
        let key = key.into();
        match self.index(&key) {
            Some(index) => {
                let node = self.table[index].as_ref().unwrap();
                return Some(&node.value);
            },
            None => None,
        }
    }

    /// removes the value associated with the given key and returns the removed value or None if
    /// there was no value associated with the given key
    pub fn remove<A: Into<K>>(&mut self, key: A) -> Option<V> {
        let key = key.into();
        match self.index(&key) {
            Some(index) => {
                self.size -= 1;
                let removed_node = mem::replace(&mut self.table[index], None).unwrap();
                Some(removed_node.value)
            },
            None => None,
        }
    }

    pub fn size(&self) -> usize {
        self.size as usize
    }

    /// returns the index of the given key in the backing array, or returns None if the key isn't
    /// in this hash table
    fn index(&self, key: &K) -> Option<usize> {
        let mut index = self.hash(key) as usize;
        loop {
            match self.table[index] {
                Some(ref node) =>  {
                    if node.key == *key {
                        return Some(index);
                    }
                },
                None => return None,
            }

            index += 1;
            if index >= self.table.len() {
                index = 0;
            }
        }
    }

    /// Returns this table's hash for the given key. Note that hash is dependent on the size of the
    /// backing vector so a key's hash will change when the backing vector is resized.
    fn hash(&self, key: &K) -> u64 {
        let mut hasher = DefaultHasher::new();
        key.hash(&mut hasher);
        let hash = hasher.finish();

        // scale the hash to our current capacity
        hash % (self.table.len() as u64)
    }

    /// the load factor of this hash table, defined as used buckets divided by total number of
    /// buckets
    fn load_factor(&self) -> f32 {
        (self.size as f32)/(self.table.len() as f32)
    }

    /// Grows the backing vector by a factor of 2 and redistributes the buckets across the new
    /// capacity to reduce load factor.
    fn grow_storage(&mut self) {
        let new_capacity = self.table.len() * 2;
        let mut new_table = Vec::with_capacity(0);
        new_table.resize_default(new_capacity);
        let old_table = mem::replace(&mut self.table, new_table);
        self.size = 0;

        // note that all the hashes have been invalidated by resizing the backing vector since the
        // backing vector's size is a factor in the hash computation
        for bucket in old_table.into_iter().filter_map(|bucket| bucket) {
            self.insert_raw(bucket.key, bucket.value);
        }
    }

    /// returns the number of buckets this element is removed from its ideal position
    fn displacement(hash: usize, index: usize, table_capacity: usize) -> usize {
        if hash <= index {
            index - hash
        } else {
            (table_capacity - hash) + index
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_insertion() {
        let mut hash_table: HashTable<String, String> = HashTable::new();
        assert_eq!(hash_table.size(), 0);
        hash_table.insert("a", "Monday");
        assert_eq!(hash_table.size(), 1);
        assert_eq!(*hash_table.get("a").unwrap(), "Monday");
    }

    #[test]
    fn test_overwrite() {
        let mut hash_table: HashTable<String, String> = HashTable::new();
        assert_eq!(hash_table.size(), 0);
        hash_table.insert("a", "Monday");
        assert_eq!(hash_table.size(), 1);
        hash_table.insert("a", "Tuesday");
        assert_eq!(hash_table.size(), 1);
        assert_eq!(*hash_table.get("a").unwrap(), "Tuesday");
    }

    #[test]
    fn test_get_nonexistent_key() {
        let hash_table: HashTable<String, String> = HashTable::new();
        assert_eq!(hash_table.get("a"), None);
    }

    #[test]
    fn test_get_removed_key() {
        let mut hash_table: HashTable<String, String> = HashTable::new();
        assert_eq!(hash_table.size(), 0);
        hash_table.insert("a", "Monday");
        assert_eq!(hash_table.size(), 1);
        assert_eq!(*hash_table.get("a").unwrap(), "Monday");
        hash_table.remove("a");
        assert_eq!(hash_table.size(), 0);
        assert_eq!(hash_table.get("a"), None);
    }

    #[test]
    fn test_with_many_keys() {
        let num_items_to_insert = 1_000_000u32;

        let mut hash_table: HashTable<String, u32> = HashTable::new();
        assert_eq!(hash_table.size(), 0);
        for count in 0..num_items_to_insert {
            hash_table.insert(count.to_string(), count);
        }
        assert_eq!(hash_table.size(), num_items_to_insert as usize);
        assert_eq!(hash_table.get("-1"), None);
        assert_eq!(hash_table.get("10"), Some(&10));
        hash_table.remove("10");
        assert_eq!(hash_table.size(), (num_items_to_insert - 1) as usize);
        assert_eq!(hash_table.get("10"), None);
    }
}
