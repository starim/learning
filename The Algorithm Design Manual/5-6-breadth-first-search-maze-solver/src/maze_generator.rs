use std::collections::HashMap;

use petgraph::graph::NodeIndex;
use petgraph::Graph;
use rand::thread_rng;
use rand::seq::SliceRandom;

use super::{MazeGraph, NodeIndexMap};

/// creates a random maze with the given height and width using the random backtracking method
/// inspired by this implementation:
/// https://weblog.jamisbuck.org/2010/12/27/maze-generation-recursive-backtracking
pub fn maze_graph<F: FnMut(&MazeGraph)>(
    width: u16,
    height: u16,
    mut step_callback: F,
) -> (MazeGraph, NodeIndexMap) {
    let mut graph: MazeGraph =
        Graph::with_capacity((width * height) as usize, (width * height * 3) as usize);

    let mut node_indices: HashMap<(u16, u16), NodeIndex<u16>> =
        HashMap::with_capacity((width * height) as usize);
    for x in 0..width {
        for y in 0..height {
            let key = graph.add_node((x, y));
            node_indices.insert((x, y), key);
        }
    }

    let mut seen_nodes: Vec<bool> = vec![false; (width * height) as usize];
    maze_generation_step_depth_first(
        &mut graph,
        &node_indices,
        width,
        height,
        0,
        0,
        &mut seen_nodes,
        &mut step_callback,
    );

    (graph, node_indices)
}

fn maze_generation_step_depth_first<F: FnMut(&MazeGraph)>(
    graph: &mut MazeGraph,
    node_indices: &HashMap<(u16, u16), NodeIndex<u16>>,
    width: u16,
    height: u16,
    x: u16,
    y: u16,
    seen_nodes: &mut Vec<bool>,
    step_callback: &mut F,
) {
    step_callback(graph);

    let mut rng = thread_rng();
    let mut directions = [(0, 1), (0, -1), (1, 0), (-1, 0)];
    directions.shuffle(&mut rng);
    let directions = directions;

    for step in directions.iter() {
        let new_x_i32 = (x as i32) + step.0;
        let new_y_i32 = (y as i32) + step.1;

        if
            new_x_i32 >= 0 &&
            new_x_i32 < (width as i32) &&
            new_y_i32 >= 0 &&
            new_y_i32 < (height as i32) &&
            !seen_nodes[((height as i32) * new_x_i32 + new_y_i32) as usize]
        {
            // because we've verified that each new coordinate is in the range [0, width] or [0,
            // height], casting back to u16 is safe
            let new_x = new_x_i32 as u16;
            let new_y = new_y_i32 as u16;

            seen_nodes[(height * new_x + new_y) as usize] = true;
            let old_node = node_indices.get(&(x, y)).expect("No node index found.");
            let next_node = node_indices.get(&(new_x, new_y)).expect("No node index found.");
            graph.update_edge(*old_node, *next_node, ());

            maze_generation_step_depth_first(
                graph,
                node_indices,
                width,
                height,
                new_x,
                new_y,
                seen_nodes,
                step_callback,
            );
        }
    }
}
