use std::collections::HashMap;
use std::path::Path;

use kiss3d::window::Window;
use kiss3d::light::Light;
use kiss3d::scene::PlanarSceneNode;
use nalgebra::geometry::Translation2;
use petgraph::graph::NodeIndex;

use super::{MazeGraph, MAZE_SIZE};

// note that we have to use upside-down coordinates because Kiss3D doesn't invert the y-axis when
// decoding png and gif images
const RECTANGLE_UVS: [[f32; 2]; 4] = [
        [1.0, 0.0],
        [0.0, 1.0],
        [0.0, 0.0],
        [1.0, 1.0],
];

pub struct MazeScene {
    window: Window,
    cubes: HashMap<(u16, u16), PlanarSceneNode>,
    window_open: bool,
}

impl MazeScene {
    pub fn new(width: u16, height: u16) -> Self {
        let mut window = Window::new("Breadth-First Maze Solution");

        MazeScene::init_lighting(&mut window);
        let mut cubes = MazeScene::init_cube_positions(&mut window, width, height);
        MazeScene::init_textures(&mut cubes);

        MazeScene {
            window,
            cubes,
            window_open: true,
        }
    }

    fn init_textures(cubes: &mut HashMap<(u16, u16), PlanarSceneNode>) {
        // Kiss3D seems to require set_texture_from_file() to be called to initialize a texture, so
        // we have to set the first cube to all textures in succession to load them
        {
            let (_, cube) = cubes.iter_mut().next().expect(
                "At least one cube must exist before initializing textures."
            );
            cube.set_texture_from_file(&Path::new("./assets/texture_no_walls.png"), "wall_none");
            cube.set_texture_from_file(&Path::new("./assets/texture_wall_all.png"), "wall_lrud");
            cube.set_texture_from_file(&Path::new("./assets/texture_wall_bottom.png"), "wall_d");
            cube.set_texture_from_file(&Path::new("./assets/texture_wall_left_bottom.png"), "wall_ld");
            cube.set_texture_from_file(&Path::new("./assets/texture_wall_left.png"), "wall_l");
            cube.set_texture_from_file(&Path::new("./assets/texture_wall_left_right_bottom.png"), "wall_lrd");
            cube.set_texture_from_file(&Path::new("./assets/texture_wall_left_right.png"), "wall_lr");
            cube.set_texture_from_file(&Path::new("./assets/texture_wall_left_right_top.png"), "wall_lru");
            cube.set_texture_from_file(&Path::new("./assets/texture_wall_left_top_bottom.png"), "wall_lud");
            cube.set_texture_from_file(&Path::new("./assets/texture_wall_left_top.png"), "wall_lu");
            cube.set_texture_from_file(&Path::new("./assets/texture_wall_right_bottom.png"), "wall_rd");
            cube.set_texture_from_file(&Path::new("./assets/texture_wall_right.png"), "wall_r");
            cube.set_texture_from_file(&Path::new("./assets/texture_wall_right_top_bottom.png"), "wall_rud");
            cube.set_texture_from_file(&Path::new("./assets/texture_wall_right_top.png"), "wall_ru");
            cube.set_texture_from_file(&Path::new("./assets/texture_wall_top_bottom.png"), "wall_ud");
            cube.set_texture_from_file(&Path::new("./assets/texture_wall_top.png"), "wall_u");
        }

        // now initialize the cubes with the blank "no walls" texture
        for (_, cube) in cubes.iter_mut() {
            cube.set_texture_with_name("wall_none");
        }
    }

    fn init_cube_positions(window: &mut Window, width: u16, height: u16) -> HashMap<(u16, u16), PlanarSceneNode> {
        let mut cubes: HashMap<(u16, u16), PlanarSceneNode> =
            HashMap::with_capacity(MAZE_SIZE as usize);
        for x in 0..width {
            for y in 0..height {
                // TODO: scale needs to adjust based on the height and width of the maze--the
                // larger the maze, the smaller the scale
                let scale = 15.0;
                let mut cube = window.add_rectangle(scale, scale);
                let screen_x = ((x as f32) - (width as f32)/2.0) * scale;
                let screen_y = (-(y as f32) + (height as f32)/2.0) * scale;
                cube.append_translation(&Translation2::new(screen_x, screen_y));
                cube.modify_uvs(&mut |points|{
                    for(i, p) in points.iter_mut().enumerate() {
                        p[0] = RECTANGLE_UVS[i][0];
                        p[1] = RECTANGLE_UVS[i][1];
                    }
                });
                cubes.insert((x, y), cube);
            }
        }

        cubes
    }

    fn init_lighting(window: &mut Window) {
        window.set_light(Light::StickToCamera);
    }

    pub fn render(
        &mut self,
        maze: &MazeGraph,
        colors_option: Option<&HashMap<(u16, u16), (f32, f32, f32)>>,
    ) -> bool {
        if !self.window_open {
            return false;
        }


        if let Some(colors) = colors_option {
            for (&(x, y), &(r, g, b)) in colors.iter() {
                self.cubes
                    .get_mut(&(x, y))
                    .expect("No cube exists for this grid coordinate.")
                    .set_color(r, g, b);
            }
        }

        for node in maze.node_indices() {
            let &(x, y) = maze.node_weight(node).unwrap();

            self.cubes
                .get_mut(&(x, y))
                .expect("No cube exists for this grid coordinate.")
                .set_texture_with_name(MazeScene::texture_for_node(maze, node));
        }

        self.window_open = self.window.render();
        self.window_open
    }

    fn texture_for_node(maze: &MazeGraph, node: NodeIndex<u16>) -> &'static str {

        struct NodeWalls {
            pub up: bool,
            pub down: bool,
            pub left: bool,
            pub right: bool,
        }

        let mut walls = NodeWalls { up: true, down: true, left: true, right: true };

        let &(x, y) = maze.node_weight(node).unwrap();

        for neighbor in maze.neighbors(node) {
            let &(neighbor_x, neighbor_y) = maze.node_weight(neighbor).unwrap();
            let relative_position = (
                (x as i32) - (neighbor_x as i32),
                (y as i32) - (neighbor_y as i32),
            );
            match relative_position {
                (1, 0) => walls.left = false,
                (-1, 0) => walls.right = false,
                (0, 1) => walls.up = false,
                (0, -1) => walls.down = false,
                (x, y) => {
                    panic!(
                        "Invalid neighbor relative position ({}, {}). This is probably the \
                        result of a cell having an edge to a non-adjacent cell in the maze.",
                        x,
                        y,
                    );
                },
            }
        }

        let walls = walls;

        match walls {
            NodeWalls { up: false, down: false, left: false, right: false } => {
                "wall_none"
            },
            NodeWalls { up: false, down: false, left: false, right: true } => {
                "wall_r"
            },
            NodeWalls { up: false, down: false, left: true, right: false } => {
                "wall_l"
            },
            NodeWalls { up: false, down: false, left: true, right: true } => {
                "wall_lr"
            },
            NodeWalls { up: false, down: true, left: false, right: false } => {
                "wall_d"
            },
            NodeWalls { up: false, down: true, left: false, right: true } => {
                "wall_rd"
            },
            NodeWalls { up: false, down: true, left: true, right: false } => {
                "wall_ld"
            },
            NodeWalls { up: false, down: true, left: true, right: true } => {
                "wall_lrd"
            },
            NodeWalls { up: true, down: false, left: false, right: false } => {
                "wall_u"
            },
            NodeWalls { up: true, down: false, left: false, right: true } => {
                "wall_ru"
            },
            NodeWalls { up: true, down: false, left: true, right: false } => {
                "wall_lu"
            },
            NodeWalls { up: true, down: false, left: true, right: true } => {
                "wall_lru"
            },
            NodeWalls { up: true, down: true, left: false, right: false } => {
                "wall_ud"
            },
            NodeWalls { up: true, down: true, left: false, right: true } => {
                "wall_rud"
            },
            NodeWalls { up: true, down: true, left: true, right: false } => {
                "wall_lud"
            },
            NodeWalls { up: true, down: true, left: true, right: true } => {
                "wall_lrud"
            },
        }
    }
}
