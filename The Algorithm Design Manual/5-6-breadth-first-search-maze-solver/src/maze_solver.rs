use std::collections::HashMap;
use std::collections::VecDeque;

use super::{MazeGraph, MAZE_SIZE, NodeIndexMap};

#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub enum DiscoveryState {
    Undiscovered,
    Discovered,
    Processed,
}

#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub struct Vertex {
    pub parent: Option<(u16, u16)>,
    pub state: DiscoveryState,
}

pub fn breadth_first_search<F: FnMut(&HashMap<(u16, u16), Vertex>, (u16, u16))>(
    graph: &MazeGraph,
    graph_index_map: NodeIndexMap,
    width: u16,
    height: u16,
    start_node: (u16, u16),
    mut before_vertex_processed: Option<F>,
    mut after_vertex_processed: Option<F>,
) -> HashMap<(u16, u16), Vertex> {
    let mut vertices = new_vertex_data(width, height);
    let mut queue: VecDeque<(u16, u16)> = VecDeque::new();
    queue.push_back(start_node);
    {
        let mut start_node_data =
            vertices
            .get_mut(&start_node)
            .expect("Supplied start node doesn't exist in vertex data");
        start_node_data.state = DiscoveryState::Discovered;
    }

    while !queue.is_empty() {
        let current_node = queue.pop_front().expect("The queue can't be empty here.");
        if let Some(ref mut callback) = before_vertex_processed {
            callback(&vertices, current_node);
        }

        let mut vertex_data = vertices
            .get_mut(&current_node)
            .expect("Node index in queue doesn't exist in vertex data.");
        vertex_data.state = DiscoveryState::Processed;

        let graph_index =
            graph_index_map
            .get(&current_node)
            .expect("Graph index map does't have an entry for this node.");

        for graph_index_child in graph.neighbors(*graph_index) {
            let child_node = graph.node_weight(graph_index_child).unwrap();
            let mut child_vertex_data =
                vertices
                .get_mut(child_node)
                .expect("Neighbor node doesn't exist in vertex data.");
            // to be implemented
            // if child_vertex_data.state != DiscoveryState::Processed {
            //     process_edge(vertices, child_node);
            // }
            if child_vertex_data.state == DiscoveryState::Undiscovered {
                queue.push_back(*child_node);
                child_vertex_data.state = DiscoveryState::Discovered;
                child_vertex_data.parent = Some(current_node);
            }
        }
        if let Some(ref mut callback) = after_vertex_processed {
            callback(&vertices, current_node);
        }
    }

    vertices
}

fn new_vertex_data(width: u16, height: u16) -> HashMap<(u16, u16), Vertex> {
    let mut vertices: HashMap<(u16, u16), Vertex> =
        HashMap::with_capacity(MAZE_SIZE as usize);
    for x in 0..width {
        for y in 0..height {
            vertices.insert(
                (x, y),
                Vertex { parent: None, state: DiscoveryState::Undiscovered },
            );
        }
    }

    vertices
}

pub fn solution<F: FnMut(&Vec<(u16, u16)>)>(
    vertex_data: &HashMap<(u16, u16), Vertex>,
    width: u16,
    height: u16,
    mut step_callback: F,
) -> Vec<(u16, u16)> {
    let exit = (width - 1, height - 1);
    let mut solution: Vec<(u16, u16)> = vec![exit];

    let mut next_node = exit;
    while let Some(parent) = vertex_data.get(&next_node).unwrap().parent {
        solution.push(parent);
        next_node = parent;

        step_callback(&solution);
    }

    solution
}
