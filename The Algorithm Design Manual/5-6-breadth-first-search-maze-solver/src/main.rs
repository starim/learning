// This implementation is based on the one on p. 162-163 of The Algorithm Design Manual, but the
// two boolean arrays tracking whether vertices are in the discovered or processed state have been
// replaced with a single array of enum values.

// height and width of the maze
const HEIGHT: u16 = 25;
const WIDTH: u16 = 50;
const MAZE_SIZE: u16 = HEIGHT * WIDTH;
const UNDISCOVERED_COLOR: (f32, f32, f32) = (1.0, 1.0, 1.0);
const DISCOVERED_COLOR: (f32, f32, f32) = (0.5, 0.0, 0.5);
const PROCESSED_COLOR: (f32, f32, f32) = (0.0, 1.0, 0.0);

const ANIMATE_MAZE_GENERATION: bool = true;
const ANIMATE_MAZE_ANALYSIS: bool = true;
const ANIMATE_SOLUTION_BUILDING: bool = true;

mod maze_generator;
mod rendering;
mod maze_solver;

use std::collections::HashMap;

use petgraph::Graph;
use petgraph::Undirected;
use petgraph::graph::NodeIndex;

use crate::rendering::MazeScene;

type MazeGraph = Graph<(u16, u16), (), Undirected, u16>;
type NodeIndexMap = HashMap<(u16, u16), NodeIndex<u16>>;

fn main() {
    let mut scene = MazeScene::new(WIDTH, HEIGHT);
    let (maze, node_index_map) = maze_generator::maze_graph(WIDTH, HEIGHT, |maze| {
        if ANIMATE_MAZE_GENERATION {
            scene.render(maze, None);
        }
    });

    let vertex_data = maze_solver::breadth_first_search(
        &maze,
        node_index_map,
        WIDTH,
        HEIGHT,
        (0, 0),
        None,
        Some(|vertex_data: &HashMap<(u16, u16), maze_solver::Vertex>, _current_node| {
            if ANIMATE_MAZE_ANALYSIS {
                let colors: HashMap<(u16, u16), (f32, f32, f32)> =
                    vertex_data
                    .iter()
                    .map(|(&node, vertex)| {
                        let color = match vertex.state {
                            maze_solver::DiscoveryState::Undiscovered => UNDISCOVERED_COLOR,
                            maze_solver::DiscoveryState::Discovered => DISCOVERED_COLOR,
                            maze_solver::DiscoveryState::Processed => PROCESSED_COLOR,
                        };
                        (node, color)
                    })
                    .collect();

                scene.render(&maze, Some(&colors));
            }
        }),
    );

    let solution = maze_solver::solution(&vertex_data, WIDTH, HEIGHT, |solution| {
        if ANIMATE_SOLUTION_BUILDING {
            let colors = solution_color(solution);
            scene.render(&maze, Some(&colors));
        }
    });
    let solved_colors = solution_color(&solution);
    while scene.render(&maze, Some(&solved_colors)) {
        std::thread::sleep(std::time::Duration::from_millis(1000));
    }
}

fn solution_color(solution: &[(u16, u16)]) -> HashMap<(u16, u16), (f32, f32, f32)> {
    let mut colors: HashMap<(u16, u16), (f32, f32, f32)> =
        HashMap::with_capacity(MAZE_SIZE as usize);

    for x in 0..WIDTH {
        for y in 0..HEIGHT {
            colors.insert((x, y), UNDISCOVERED_COLOR);
        }
    }

    for solution_node in solution {
        let node =
            colors
            .get_mut(&solution_node)
            .expect("This solution node lies outside the maze.");
        *node = DISCOVERED_COLOR;
    }

    colors
}
