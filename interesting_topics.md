= Interesting Open Source Projects

== Rust

* [Wyvern: a GOG game manager](https://github.com/nicohman/wyvern )
* [Stratis: a mangement layer that provides ZFS-like capabilities to existing Linux filesystems](https://github.com/stratis-storage)


= Computer Science

[Open Source Society University](https://github.com/ossu/computer-science)
[Teach Yourself Computer Science](https://teachyourselfcs.com/)
[Graphics Programming Articles](http://iquilezles.org/index.html)


= Graphics

[Checklist of OpenGL proficiency (see Advice 4 section at end of article](https://erkaman.github.io/posts/beginner_computer_graphics.html)

Experiments

1. [Beautiful volumetric clouds with LOD](https://www.shadertoy.com/view/XslGRr)
2. [Slit Scan effect implemented efficiently in a fragment shader](http://roy.red/slitscan-.html#slitscan)
3. [Fragment shader patterns created with blending modes](http://blog.mother4game.com/post/123598820159/update-2-focus-on-the-background)
5. [World with terrain](http://oos.moxiecode.com/js_webgl/terrain/index.html)
4. [Realistic water](http://madebyevan.com/webgl-water/)
3. Mandelbrot set rendering
4. [maps with flags](http://www.istockphoto.com/photo/south-carolina-gm157374943-6898266)
5. [Fluid Simulation](https://www.outpan.com/app/44bdd9869c/interactive-fluid-simulation)
[Article on the technique](http://developer.download.nvidia.com/books/HTML/gpugems/gpugems_ch38.html)
[WebGL code](https://github.com/PavelDoGreat/WebGL-Fluid-Simulation)
6. [Ray Tracing a Tiny Procedural Planet](https://news.ycombinator.com/item?id=21433078)

Tutorials

1. [Effective OpenGL](http://www.g-truc.net/doc/Effective%20OpenGL.pdf)
2. [Incremental OpenGL tutorials covering broad topics used in game engines using OpenGL 3.3](http://www.mbsoftworks.sk/index.php?page=tutorials&series=1)
2. [Graphics programming projects starting with basics and advancing to a real-time GPU-based ray tracer](http://graphicscodex.com/projects/projects/)
3. [Learn OpenGL](https://learnopengl.com/)
4. [Learn OpenGL Rust port](https://github.com/bwasty/learn-opengl-rs)
4. [Modern OpenGL tutorials](http://ogldev.atspace.co.uk/index.html)
7. [How to build a scene in OpenGL](https://www.youtube.com/watch?v=WfOjo9Nb018&feature=youtu.be)
8. [WebGL 2 rendering algorithms examples](https://github.com/tsherif/webgl2examples)
9. [The Book of Shaders](http://thebookofshaders.com/)
10. [MSDF font rendering in OpenGL](https://medium.com/@calebfaith/implementing-msdf-font-in-opengl-ea09a9ab7e00)
11. ["Font Rendering is Getting Interesting"](http://aras-p.info/blog/2017/02/15/Font-Rendering-is-Getting-Interesting/)
12. [Raymarching](https://iquilezles.org/www/articles/raymarchingdf/raymarchingdf.htm)
13. [Simple to Understand Ray Tracer in 256 lines of C++](https://github.com/ssloy/tinyraytracer)
14. [GLSL path tracer](https://www.reddit.com/r/programming/comments/d9xu12/a_glsl_path_tracer/)

Inspirations

1. [Infinitown](http://demos.littleworkshop.fr/infinitown)
2. [three.js effects](http://www.duhaihang.com/archive/dev-random-notes-2016/#/unfold-the-oil-paper/)
4. [heightmapped terrain example](https://github.com/MauriceGit/Energy-Dome_Terrain)
5. [Using Direct State Access](https://github.com/Fennec-kun/Guide-to-Modern-OpenGL-Functions)
6. [GPU-accelerated path tracer in Rust](https://bheisler.github.io/post/writing-gpu-accelerated-path-tracer-part-1/)
7. [Russian AI Cup--simple low-poly game for an AI competition](https://gist.github.com/kuviman/efe495b3108b56be7904144e529bad14)
8. [Coding Adventures: Ray Marching](https://www.youtube.com/watch?v=Cp5WWtMoeKg)
9. [Vector Field Visualization](https://anvaka.github.io/fieldplay/?cx=-0.8115499999999995&cy=-0.41255&w=15.8087&h=15.8087&dt=0.01&fo=0.998&dp=0.009&cm=1&vf=%2F%2F%20p.x%20and%20p.y%20are%20current%20coordinates%0A%2F%2F%20v.x%20and%20v.y%20is%20a%20velocity%20at%20point%20p%0Avec2%20get_velocity%28vec2%20p%29%20%7B%0A%20%20vec2%20v%20%3D%20vec2%280.%2C%200.%29%3B%0A%0A%20%20%2F%2F%20change%20this%20to%20get%20a%20new%20vector%20field%0A%20%20v.x%20%3D%200.2%20*%20%28%20p.y*p.y*p.y%20-%20p.x*p.x%20-%20p.y*p.y%20%2B%20p.y*p.x%20-%20p.x*p.x*p.x%20%29%3B%0A%20%20v.y%20%3D%20-0.2%20*%20%28p.y*p.y*p.x%20%20-%20p.y%20-%20p.x%20%29%3B%0A%0A%20%20return%20v%3B%0A%7D&code=%2F%2F%20p.x%20and%20p.y%20are%20current%20coordinates%0A%2F%2F%20v.x%20and%20v.y%20is%20a%20velocity%20at%20point%20p%0Avec2%20get_velocity%28vec2%20p%29%20%7B%0A%20%20vec2%20v%20%3D%20vec2%280.%2C%200.%29%3B%0A%0A%20%20%2F%2F%20change%20this%20to%20get%20a%20new%20vector%20field%0A%20%20v.x%20%3D%200.2%20*%20%28%20p.y*p.y*p.y%20-%20p.x*p.x%20-%20p.y*p.y%20%2B%20p.y*p.x%20-%20p.x*p.x*p.x%20%29%3B%0A%20%20v.y%20%3D%20-0.2%20*%20%28p.y*p.y*p.x%20%20-%20p.y%20-%20p.x%20%29%3B%0A%0A%20%20return%20v%3B%0A%7D)
10. [Many interesting simulations and math visualizations](https://twitter.com/search?q=from%3Aanvaka%20min_retweets%3A20&src=typed_query)

= Misc.

[Implementing TCP in Rust](https://www.youtube.com/watch?v=bzja9fQWzdA)
[World of Warcraft addon programming](https://www.reddit.com/r/classicwow/comments/d9jbki/a_beginners_guide_to_developing_an_addon_for/)

