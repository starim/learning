// https://adventofcode.com/2018/day/1#part2

use std::collections::HashSet;
use std::io::{BufRead, Cursor};

const INPUTS: &'static [u8] = include_bytes!("inputs");
const INITIAL_FREQUENCY: i32 = 0;

fn main() {
    let cursor = Cursor::new(INPUTS);

    let mut frequencies = Vec::new();
    let total_frequency_shift =
        cursor
        .lines()
        .map(|line| {
            line
                .expect("Failed to read line")
                .trim()
                .parse::<i32>()
                .expect("Couldn't parse line into an integer")
        })
        .fold(INITIAL_FREQUENCY, |accum, shift| {
            let new_frequency = accum + shift;
            frequencies.push(new_frequency);
            new_frequency
        });
    let frequencies = frequencies;

    println!("Repeat frequency: {}", repeat_frequency(&frequencies, total_frequency_shift));
}

/// returns the first frequency to appear twice
fn repeat_frequency(frequencies: &Vec<i32>, total_frequency_shift: i32) -> i32 {
    let mut num_occurrences = HashSet::new();
    num_occurrences.insert(0);
    let mut num_loops = 0;
    loop {
        for base_frequency in frequencies.iter() {
            let frequency = base_frequency + (num_loops * total_frequency_shift);
            //println!("frequency: {} (base: {}, num_loops: {})", frequency, base_frequency, num_loops);
            match num_occurrences.get(&frequency) {
                Some(_) => return frequency,
                None => num_occurrences.insert(frequency),
            };
        }

        num_loops += 1;
    }
}
