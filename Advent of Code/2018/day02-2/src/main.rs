// https://adventofcode.com/2018/day/2#part2

use std::io::{BufRead, Cursor};

fn inputs() -> Vec<String> {
    let inputs: &'static [u8] = include_bytes!("input");
    Cursor::new(inputs)
        .lines()
        .map(|line| line.expect("Failed to read line").trim().to_owned())
        .collect()
}

fn matching_ids(ids: &Vec<String>) -> Option<(&str, &str)> {
    for i in 0..ids.len() {
        for j in 0..ids.len() {
            if i == j {
                continue;
            }

            let num_mismatching_chars =
                ids[i].chars().zip(ids[j].chars())
                .fold(0, |mismatch_count, (char_1, char_2)| {
                    if char_1 == char_2 {
                        mismatch_count
                    } else {
                        mismatch_count + 1
                    }
                });

            if num_mismatching_chars == 1 {
                return Some((&ids[i], &ids[j]));
            }
        }
    }

    return None;
}

fn main() {
    let ids = inputs();
    let matches = matching_ids(&ids).expect("Couldn't find any matching IDs");
    let matching_characters =
        matches.0.chars().zip(matches.1.chars())
        .filter_map(|(char_1, char_2)| {
            if char_1 == char_2 {
                Some(char_1)
            } else {
                None
            }
        })
        .collect::<String>();

    println!(
        "box IDs:\n{}\n{}\n",
        matches.0,
        matches.1,
    );

    println!("matching characters: {}", matching_characters);
}
