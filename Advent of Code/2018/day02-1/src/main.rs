// https://adventofcode.com/2018/day/2

use std::collections::HashMap;
use std::io::{BufRead, Cursor};

fn inputs() -> Vec<String> {
    let inputs: &'static [u8] = include_bytes!("input");
    Cursor::new(inputs)
        .lines()
        .map(|line| line.expect("Failed to read line").trim().to_owned())
        .collect()
}

fn checksum(ids: Vec<String>) -> i32 {
    let counts = ids.iter().fold((0, 0), |last_count, id| {
        let mut counts = last_count;
        let mut num_occurrences: HashMap<char, i32> = HashMap::new();
        for letter in id.chars() {
            let counter = num_occurrences.entry(letter).or_insert(0);
            *counter += 1;
        }

        if num_occurrences.iter().filter(|(_, &occurrences)| occurrences == 2).count() > 0 {
            counts.0 += 1;
        }
        if num_occurrences.iter().filter(|(_, &occurrences)| occurrences == 3).count() > 0 {
            counts.1 += 1;
        }

        counts
    });

    counts.0 * counts.1
}

fn main() {
    let ids = inputs();

    println!("checksum: {}", checksum(ids));
}
