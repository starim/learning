// https://adventofcode.com/2018/day/1

use std::io::{BufRead, Cursor};

const INPUTS: &'static [u8] = include_bytes!("inputs");

fn main() {
    let cursor = Cursor::new(INPUTS);

    let total_frequency_shift =
        cursor.lines().map(|line| {
            line
                .expect("Failed to read line")
                .trim()
                .parse::<i32>()
                .expect("Couldn't parse line into an integer")
        }).sum::<i32>();

    println!("Total frequency shift: {}", total_frequency_shift);
}
