// https://adventofcode.com/2018/day/3#part2

use std::io::{BufRead, Cursor};

type ID = u32;

fn inputs() -> Vec<Claim> {
    let inputs: &'static [u8] = include_bytes!("input");
    Cursor::new(inputs)
        .lines()
        .map(|line| {
            let mut line =
                line
                .expect("Failed to read line")
                .trim()
                .to_owned();
            line.retain(|character| character != ' ');

            let mut line = line.splitn(2, "@");
            let id_string = line.next().unwrap();
            let id =
                id_string[1..]
                .parse::<u32>()
                .expect("Couldn't parse ID string into an unsigned integer ID");

            let mut line = line.next().unwrap().splitn(2, ":");
            let top_left_string = line.next().unwrap();
            let top_left =
                top_left_string
                .splitn(2, ",")
                .map(|coord|
                    coord
                    .parse::<usize>()
                    .expect("Couldn't parse top-left claim coordinate into an unsigned integer")
                ).collect::<Vec<usize>>();
            let top_left = (top_left[0], top_left[1]);

            let dimensions =
                line
                .next()
                .unwrap()
                .splitn(2, "x")
                .map(|dimension_string| {
                    dimension_string
                    .parse::<usize>()
                    .expect("Couldn't parse width into an unsigned integer.")
                }).collect::<Vec<usize>>();
            let width = dimensions[0];
            let height = dimensions[1];

            Claim { id, top_left, width, height }
        })
        .collect()
}

#[derive(Debug, Copy, Clone, Eq, PartialEq)]
struct Claim {
    id: ID,
    top_left: (usize, usize),
    width: usize,
    height: usize,
}

fn mark_used_squares(cloth: &mut Vec<Vec<u32>>, claim: &Claim) {
    for x in claim.top_left.0..(claim.top_left.0 + claim.width) {
        for y in claim.top_left.1..(claim.top_left.1 + claim.height) {
            cloth[x][y] += 1;
        }
    }
}

fn unique_id(cloth: &Vec<Vec<u32>>, claims: Vec<Claim>) -> ID {
    let unique_claims: Vec<Claim> =
        claims
        .into_iter()
        .filter(|claim| {
            for x in claim.top_left.0..(claim.top_left.0 + claim.width) {
                for y in claim.top_left.1..(claim.top_left.1 + claim.height) {
                    assert!(cloth[x][y] != 0);
                    if cloth[x][y] > 1 {
                        return false;
                    }
                }
            }

            true
        })
        .collect();
    assert_eq!(unique_claims.len(), 1);
    unique_claims[0].id
}

fn main() {
    let claims = inputs();
    let max_x = claims.iter().map(|claim| claim.top_left.0 + claim.width).max().expect("No claims exist");
    let max_y = claims.iter().map(|claim| claim.top_left.1 + claim.height).max().expect("No claims exist");

    let mut cloth: Vec<Vec<u32>> = Vec::with_capacity(max_x);
    for _ in 0..max_x {
        let mut column: Vec<u32> = Vec::with_capacity(max_y);
        for _ in 0..max_y {
            column.push(0);
        }
        cloth.push(column);
    }

    for claim in claims.iter() {
        mark_used_squares(&mut cloth, claim);
    }

    println!("unique ID: {}", unique_id(&cloth, claims));
}
