use std::os::raw::c_void;
use std::path::Path;

use gl;
use self::gl::types::*;

use image;
use image::GenericImage;
use image::DynamicImage::*;

/// describes the raw images that are to be used to create a skybox
#[derive(Debug)]
pub struct RawSkyboxImages {
    pub right: String,
    pub left: String,
    pub top: String,
    pub bottom: String,
    pub back: String,
    pub front: String,
}

/// takes the path to an image file, loads the image into a texture optionally flipping the image
/// on the y-axis (useful for PNG images), sets the desired texture wrapping mode, and returns the
/// texture's texture ID
pub fn load_texture(path: &str, flip_y: bool, texture_wrap_mode: GLuint) -> GLuint {
    let mut texture_id = 0;

    unsafe {
        gl::GenTextures(1, &mut texture_id);
        gl::BindTexture(gl::TEXTURE_2D, texture_id);

        load_image(
            &Path::new(path),
            gl::TEXTURE_2D,
            flip_y,
            texture_wrap_mode,
            gl::LINEAR_MIPMAP_LINEAR,
            gl::LINEAR,
            true,
        );

        gl::BindTexture(gl::TEXTURE_2D, 0);
    }

    texture_id
}

/// takes a HashMap mapping keys "front", "back", "top", "bottom", "left", and "right" to the file
/// paths of the corresponding images that make up each face of the cubemap, stitches them into a
/// single cubemap, and returns the cubemap's texture ID
pub fn load_cubemap(paths: RawSkyboxImages) -> GLuint {
    let mut texture_id = 0;

    unsafe {
        gl::GenTextures(1, &mut texture_id);
        gl::BindTexture(gl::TEXTURE_CUBE_MAP, texture_id);

        load_image(
            Path::new(&paths.right),
            gl::TEXTURE_CUBE_MAP_POSITIVE_X,
            false,
            gl::CLAMP_TO_EDGE,
            gl::LINEAR,
            gl::LINEAR,
            false,
        );
        load_image(
            Path::new(&paths.left),
            gl::TEXTURE_CUBE_MAP_NEGATIVE_X,
            false,
            gl::CLAMP_TO_EDGE,
            gl::LINEAR,
            gl::LINEAR,
            false,
        );
        load_image(
            Path::new(&paths.top),
            gl::TEXTURE_CUBE_MAP_POSITIVE_Y,
            false,
            gl::CLAMP_TO_EDGE,
            gl::LINEAR,
            gl::LINEAR,
            false,
        );
        load_image(
            Path::new(&paths.bottom),
            gl::TEXTURE_CUBE_MAP_NEGATIVE_Y,
            false,
            gl::CLAMP_TO_EDGE,
            gl::LINEAR,
            gl::LINEAR,
            false,
        );
        load_image(
            Path::new(&paths.back),
            gl::TEXTURE_CUBE_MAP_POSITIVE_Z,
            false,
            gl::CLAMP_TO_EDGE,
            gl::LINEAR,
            gl::LINEAR,
            false,
        );
        load_image(
            Path::new(&paths.front),
            gl::TEXTURE_CUBE_MAP_NEGATIVE_Z,
            false,
            gl::CLAMP_TO_EDGE,
            gl::LINEAR,
            gl::LINEAR,
            false,
        );

        gl::BindTexture(gl::TEXTURE_CUBE_MAP, 0);
    }

    println!("loaded cubemap");

    texture_id
}

fn load_image(
    path: &Path,
    target: GLuint,
    flip_y: bool,
    wrap_mode: GLuint,
    min_filter: GLuint,
    mag_filter: GLuint,
    mipmap: bool,
) {
    let mut img = image::open(&path).expect(&format!("Texture {} failed to load", path.to_string_lossy()));

    if flip_y {
        img = img.flipv(); // flip loaded texture on the y-axis.
    }
    let img = img;

    let data = img.raw_pixels();

    let format = match img {
        ImageLuma8(_) => gl::RED,
        ImageLumaA8(_) => gl::RG,
        ImageRgb8(_) => gl::RGB,
        ImageRgba8(_) => gl::RGBA,
    };

    unsafe {
        gl::TexImage2D(
            target,
            0,
            format as i32,
            img.width() as i32,
            img.height() as i32,
            0,
            format,
            gl::UNSIGNED_BYTE,
            &data[0] as *const u8 as *const c_void,
        );

        if mipmap {
            gl::GenerateMipmap(gl::TEXTURE_2D);
        }

        gl::TexParameteri(gl::TEXTURE_CUBE_MAP, gl::TEXTURE_WRAP_S, wrap_mode as i32);
        gl::TexParameteri(gl::TEXTURE_CUBE_MAP, gl::TEXTURE_WRAP_T, wrap_mode as i32);
        gl::TexParameteri(gl::TEXTURE_CUBE_MAP, gl::TEXTURE_WRAP_R, wrap_mode as i32);
        gl::TexParameteri(gl::TEXTURE_CUBE_MAP, gl::TEXTURE_MIN_FILTER, min_filter as i32);
        gl::TexParameteri(gl::TEXTURE_CUBE_MAP, gl::TEXTURE_MAG_FILTER, mag_filter as i32);
    }

    println!("loaded texture {}", path.to_string_lossy());
}
