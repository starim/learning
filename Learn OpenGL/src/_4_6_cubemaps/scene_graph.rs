use cgmath::{Deg, Matrix4, perspective};

extern crate gl;
use self::gl::types::*;

use common::camera::Camera;
use common::texture::{load_cubemap, RawSkyboxImages};

use super::light::{Light, LightType};
use super::scene_element::SceneElement;
use super::skybox::Skybox;

pub struct SceneGraph {
    projection_matrix: Matrix4<f32>,
    pub camera: Camera,

    elements: Vec<Box<SceneElement>>,
    skybox: Option<Box<SceneElement>>,
    lights: Vec<Light>,

    skybox_texture_id: Option<GLuint>,
}

impl SceneGraph {
    pub fn new(
        screen_width: u32,
        screen_height: u32,
        src_folder: &str,
        camera: Camera,
        lights: Vec<Light>,
    ) -> Self {
        // TODO: shouldn't this update when the window's resized?
        let projection_matrix: Matrix4<f32> =
            perspective(Deg(45.0), screen_width as f32 / screen_height as f32, 0.1, 100.0);

        let skybox_texture_id = load_cubemap(RawSkyboxImages {
            right: format!("{}/skybox/right.jpg", src_folder),
            left: format!("{}/skybox/left.jpg", src_folder),
            top: format!("{}/skybox/top.jpg", src_folder),
            bottom: format!("{}/skybox/bottom.jpg", src_folder),
            back: format!("{}/skybox/back.jpg", src_folder),
            front: format!("{}/skybox/front.jpg", src_folder),
        });

        let mut scene_graph = SceneGraph {
            projection_matrix: projection_matrix,
            camera: camera,
            elements: Vec::new(),
            skybox: None,
            skybox_texture_id: Some(skybox_texture_id),
            lights: lights,
        };


        scene_graph.add_skybox(Box::new(Skybox::new(src_folder)));

        scene_graph
    }

    /// registers a renderable object with the scene graph
    pub fn add_element(&mut self, element: Box<SceneElement>) {
        self.elements.push(element);
    }

    /// registers a skybox, which is given a special order in the rendering loop
    pub fn add_skybox(&mut self, skybox: Box<SceneElement>) {
        self.skybox = Some(skybox);
    }

    pub fn render_frame(&mut self, t: f32) {

        self.update_flashlights();

        unsafe {
            gl::ClearColor(0.1, 0.15, 0.15, 1.0);
            gl::Clear(gl::COLOR_BUFFER_BIT | gl::DEPTH_BUFFER_BIT | gl::STENCIL_BUFFER_BIT);
        }

        for element in self.elements.iter() {
            element.render_frame(
                t,
                &self.lights,
                &self.camera.view_matrix(),
                &self.projection_matrix,
                self.skybox_texture_id,
            );
        }

        if let Some(ref skybox) = self.skybox {
            unsafe {
                gl::Enable(gl::DEPTH_TEST);
                gl::DepthFunc(gl::LEQUAL);
            }

            skybox.render_frame(
                t,
                &self.lights,
                &self.camera.view_matrix(),
                &self.projection_matrix,
                self.skybox_texture_id,
            );

            unsafe {
                gl::DepthFunc(gl::LESS);
            }
        }
    }

    /// update all flashlights to point forward from the camera
    fn update_flashlights(&mut self) {
        let flashlights = self.lights.iter_mut().filter(|light| {
            match light.light_type {
                LightType::Spotlight { flashlight, ..} => flashlight,
                _ => false,
            }
        });
        for light in flashlights {
            match light.light_type {
                LightType::Spotlight { ref mut direction, .. } => {
                    *direction = self.camera.front.extend(0.0);
                    light.position = self.camera.position.to_homogeneous();
                },
                _ => {}
            }
        }
    }
}
