#version 330 core

out vec4 fragColor;

in vec3 normal;
in vec3 vertexPositionView;

uniform samplerCube skybox;

void main()
{
    float ratio = 1.00 / 1.52;
    vec3 I = normalize(vertexPositionView);
    vec3 R = refract(I, normalize(normal), ratio);
    fragColor = vec4(texture(skybox, R).rgb, 1.0);
}
