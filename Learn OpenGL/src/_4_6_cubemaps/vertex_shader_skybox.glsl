#version 330 core

layout (location = 0) in vec3 aPos;

uniform mat4 viewMatrix;
uniform mat4 projectionMatrix;

out vec3 textureDirection;

void main()
{
    textureDirection = aPos;
    vec4 rawPosition = projectionMatrix * viewMatrix * vec4(aPos, 1.0);
	gl_Position = rawPosition.xyww;
} 
