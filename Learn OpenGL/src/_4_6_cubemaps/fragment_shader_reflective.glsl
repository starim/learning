#version 330 core

out vec4 fragColor;

in vec3 normal;
in vec3 vertexPositionView;

uniform samplerCube skybox;

void main()
{
    vec3 I = normalize(vertexPositionView);
    vec3 R = reflect(I, normalize(normal));
    fragColor = vec4(texture(skybox, R).rgb, 1.0);
}
