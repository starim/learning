extern crate gl;
use self::gl::types::*;

use cgmath::Matrix4;

use super::light::Light;

pub trait PostProcessingEffect {
    fn render_frame(
        &self,
        t: f32,
        lights: &Vec<Light>,
        view_matrix: &Matrix4<f32>,
        projection_matrix: &Matrix4<f32>,
        framebuffer_id: GLuint,
        texture_id: GLuint,
    );
}
