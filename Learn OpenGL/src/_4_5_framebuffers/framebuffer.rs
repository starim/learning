use std::ptr;

extern crate gl;
use self::gl::types::*;

use cgmath::Matrix4;

use super::light::Light;
use super::post_processing_effect::PostProcessingEffect;
use super::scene_element::SceneElement;

pub struct Framebuffer {
    framebuffer_id: GLuint,
    texture_id: GLuint,
    clear_color: [f32; 4],
    elements: Vec<Box<SceneElement>>,
    post_processing_effect: Box<PostProcessingEffect>,
}

impl Framebuffer {
    pub fn new(
        screen_width: u32,
        screen_height: u32,
        clear_color: [f32; 4],
        post_processing_effect: Box<PostProcessingEffect>,
    ) -> Self {
        let (framebuffer_id, texture_id) =
            Framebuffer::init_opengl(screen_width, screen_height);

        Framebuffer {
            framebuffer_id: framebuffer_id,
            texture_id: texture_id,
            clear_color: clear_color,
            elements: Vec::new(),
            post_processing_effect: post_processing_effect,
        }
    }

    fn init_opengl(screen_width: u32, screen_height: u32) -> (GLuint, GLuint) {
        let mut framebuffer = 0;
        let mut texture_color_buffer = 0;

        unsafe {
            gl::GenFramebuffers(1, &mut framebuffer);
            gl::BindFramebuffer(gl::FRAMEBUFFER, framebuffer);
            gl::GenTextures(1, &mut texture_color_buffer);
            gl::BindTexture(gl::TEXTURE_2D, texture_color_buffer);
            // TODO: consider making this section a method in the texture module in common
            gl::TexImage2D(gl::TEXTURE_2D, 0, gl::RGB as i32, screen_width as i32, screen_height as i32, 0, gl::RGB, gl::UNSIGNED_BYTE, ptr::null());
            gl::TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_MIN_FILTER, gl::LINEAR as i32);
            gl::TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_MAG_FILTER, gl::LINEAR as i32);
            gl::FramebufferTexture2D(gl::FRAMEBUFFER, gl::COLOR_ATTACHMENT0, gl::TEXTURE_2D, texture_color_buffer, 0);

            let mut rbo = 0;
            gl::GenRenderbuffers(1, &mut rbo);
            gl::BindRenderbuffer(gl::RENDERBUFFER, rbo);
            // use a single renderbuffer object for both a depth AND stencil buffer.
            gl::RenderbufferStorage(gl::RENDERBUFFER, gl::DEPTH24_STENCIL8, screen_width as i32, screen_height as i32);
            gl::FramebufferRenderbuffer(gl::FRAMEBUFFER, gl::DEPTH_STENCIL_ATTACHMENT, gl::RENDERBUFFER, rbo);

            if gl::CheckFramebufferStatus(gl::FRAMEBUFFER) != gl::FRAMEBUFFER_COMPLETE {
                println!("ERROR::FRAMEBUFFER:: Framebuffer is not complete!");
            }

            gl::BindFramebuffer(gl::FRAMEBUFFER, 0);
        }

        (framebuffer, texture_color_buffer)
    }

    /// registers a renderable object that should be rendered onto this framebuffer
    pub fn add_element(&mut self, element: Box<SceneElement>) {
        self.elements.push(element);
    }
}

impl SceneElement for Framebuffer {

    /// renders a frame, optionally returning a framebuffer ID if it rendered to a framebuffer that
    /// a post-processing effect can use
    fn render_frame(
        &self,
        t: f32,
        lights: &Vec<Light>,
        view_matrix: &Matrix4<f32>,
        projection_matrix: &Matrix4<f32>,
    ) {
        unsafe {
            gl::BindFramebuffer(gl::FRAMEBUFFER, self.framebuffer_id);
            gl::ClearColor(
                self.clear_color[0],
                self.clear_color[1],
                self.clear_color[2],
                self.clear_color[3],
            );
            gl::Clear(gl::COLOR_BUFFER_BIT | gl::DEPTH_BUFFER_BIT);
        }

        for element in self.elements.iter() {
            element.render_frame(t, lights, view_matrix, projection_matrix);
        }

        unsafe {
            gl::BindFramebuffer(gl::FRAMEBUFFER, 0);
        }

        self.post_processing_effect.render_frame(
            t,
            lights,
            view_matrix,
            projection_matrix,
            self.framebuffer_id,
            self.texture_id,
        );

        unsafe {
            gl::BindFramebuffer(gl::FRAMEBUFFER, 0);
        }
    }
}

impl Drop for Framebuffer {
    fn drop(&mut self) {
        unsafe {
            gl::DeleteFramebuffers(1, &self.framebuffer_id);
        }
    }
}
