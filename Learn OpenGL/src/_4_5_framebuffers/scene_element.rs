use cgmath::Matrix4;

use super::light::Light;

pub trait SceneElement {
    /// renders a frame, optionally returning a framebuffer ID if it rendered to a framebuffer that
    /// a post-processing effect can use
    fn render_frame(
        &self,
        t: f32,
        lights: &Vec<Light>,
        view_matrix: &Matrix4<f32>,
        projection_matrix: &Matrix4<f32>,
    );
}
