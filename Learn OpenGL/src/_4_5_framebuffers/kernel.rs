extern crate gl;
use self::gl::types::*;

use std::ptr;
use std::mem;
use std::os::raw::c_void;
use std::ffi::CStr;

use cgmath::Matrix4;

use common::shader::Shader;

use super::post_processing_effect::PostProcessingEffect;
use super::light::Light;

pub struct Kernel {
    shader_program: Shader,
    vao: GLuint,
    vbo: GLuint,
    kernel: [f32; 9],
}

impl Kernel {
    pub fn new(src_folder: &str, kernel: [f32; 9]) -> Self {

        let (shader_program, vao, vbo) = Kernel::init_opengl(src_folder);

        Kernel {
            shader_program: shader_program,
            vao: vao,
            vbo: vbo,
            kernel: kernel,
        }
    }

    #[allow(dead_code)]
    pub fn new_sharpen(src_folder: &str) -> Self {
        let kernel = [
            -1.0, -1.0, -1.0,
            -1.0,  9.0, -1.0,
            -1.0, -1.0, -1.0,
        ];

        Kernel::new(src_folder, kernel)
    }

    #[allow(dead_code)]
    pub fn new_blur(src_folder: &str) -> Self {
        let kernel = [
            1.0/16.0, 2.0/16.0, 1.0/16.0,
            2.0/16.0, 4.0/16.0, 2.0/16.0,
            1.0/16.0, 2.0/16.0, 1.0/16.0,
        ];

        Kernel::new(src_folder, kernel)
    }

    #[allow(dead_code)]
    pub fn new_edge_detector(src_folder: &str) -> Self {
        let kernel = [
            1.0,  1.0,  1.0,
            1.0, -8.0,  1.0,
            1.0,  1.0,  1.0,
        ];

        Kernel::new(src_folder, kernel)
    }

    fn init_opengl(src_folder: &str) -> (Shader, GLuint, GLuint) {

        let vertex_shader_path = &format!("{}/vertex_shader_post_processing.glsl", src_folder);
        let fragment_shader_path = &format!("{}/fragment_shader_post_processing.glsl", src_folder);

        unsafe {
            let shader_program = Shader::new(vertex_shader_path, fragment_shader_path);

            let vertices: [f32; 24] = [
                // the given positions are in normalized device coordinates and will not be
                // transformed by the perspective or view matrices, and there will be no model
                // matrix

                // positions // texCoords
                -1.0,  1.0,  0.0, 1.0,
                -1.0, -1.0,  0.0, 0.0,
                 1.0, -1.0,  1.0, 0.0,

                -1.0,  1.0,  0.0, 1.0,
                 1.0, -1.0,  1.0, 0.0,
                 1.0,  1.0,  1.0, 1.0
            ];

            let (mut vao, mut vbo) = (0, 0);
            gl::GenVertexArrays(1, &mut vao);
            gl::GenBuffers(1, &mut vbo);

            gl::BindVertexArray(vao);

            gl::BindBuffer(gl::ARRAY_BUFFER, vbo);
            gl::BufferData(gl::ARRAY_BUFFER,
                           (vertices.len() * mem::size_of::<GLfloat>()) as GLsizeiptr,
                           &vertices[0] as *const f32 as *const c_void,
                           gl::STATIC_DRAW);

            let stride = 4 * mem::size_of::<GLfloat>() as GLsizei;

            // positions
            gl::VertexAttribPointer(0, 3, gl::FLOAT, gl::FALSE, stride, ptr::null());
            gl::EnableVertexAttribArray(0);

            // texture coordinates
            let texture_coords_offset = (2 * mem::size_of::<GLfloat>()) as *const c_void;
            gl::VertexAttribPointer(1, 2, gl::FLOAT, gl::FALSE, stride, texture_coords_offset);
            gl::EnableVertexAttribArray(1);

            gl::BindBuffer(gl::ARRAY_BUFFER, 0);
            gl::BindVertexArray(0);

            (shader_program, vao, vbo)
        }
    }
}

impl PostProcessingEffect for Kernel {
    fn render_frame(
        &self,
        _t: f32,
        _lights: &Vec<Light>,
        _view_matrix: &Matrix4<f32>,
        _projection_matrix: &Matrix4<f32>,
        _framebuffer_id: GLuint,
        texture_id: GLuint,
    ) {
        unsafe {
            self.shader_program.use_program();
            self.shader_program.set_int(c_str!("screenTexture"), texture_id as i32);
            // self.shader_program.set_mat3fv(c_str!("kernel"), &self.kernel);
            self.shader_program.set_1fv(c_str!("kernel"), &self.kernel);
            gl::Disable(gl::DEPTH_TEST);
            gl::BindVertexArray(self.vao);
            gl::BindTexture(gl::TEXTURE_2D, texture_id);
            gl::DrawArrays(gl::TRIANGLES, 0, 6);

            gl::Enable(gl::DEPTH_TEST);
        }
    }
}

impl Drop for Kernel {
    fn drop(&mut self) {
        unsafe {
            gl::DeleteVertexArrays(1, &self.vao);
            gl::DeleteBuffers(1, &self.vbo);
        }
    }
}
