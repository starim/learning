#version 330 core

out vec4 fragColor;

in vec2 TexCoords;

uniform sampler2D screenTexture;
uniform float[9] kernel;

const float offset = 1.0 / 300.0;  

void main()
{
    vec2 offsets[9] = vec2[](
        vec2(-offset,  offset),
        vec2( 0.0f,    offset),
        vec2( offset,  offset),
        vec2(-offset,  0.0f),
        vec2( 0.0f,    0.0f),
        vec2( offset,  0.0f),
        vec2(-offset, -offset),
        vec2( 0.0f,   -offset),
        vec2( offset, -offset)
    );
    
    vec3 samples[9];
    for(int i = 0; i < 9; i++)
    {
        samples[i] = vec3(texture(screenTexture, TexCoords.st + offsets[i]));
    }
    vec3 color = vec3(0.0);
    for(int i = 0; i < 9; i++)
        color += samples[i] * kernel[i];
    
    fragColor = vec4(color, 1.0);
}
