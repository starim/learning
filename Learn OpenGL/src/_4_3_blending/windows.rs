extern crate glfw;

extern crate gl;
use self::gl::types::*;

use std::ptr;
use std::mem;
use std::os::raw::c_void;
use std::ffi::CStr;

use cgmath::{Matrix4, Vector3, Vector4};

use common::shader::Shader;
use common::texture::load_texture;

use super::scene_element::SceneElement;
use super::light::Light;

pub struct Windows {
    shader_program: Shader,
    vao: GLuint,
    vbo: GLuint,
    model_matrices: Vec<Matrix4<f32>>,
    texture_id: u32,
}

impl Windows {
    pub fn new(src_folder: &str) -> Self {

        let (shader_program, vao, vbo) = Windows::init_opengl(src_folder);

        let model_matrices: Vec<Matrix4<f32>> = vec![
            Matrix4::from_translation(Vector3::new(-1.7, 0.5, 1.0)),
            Matrix4::from_translation(Vector3::new(-1.0, 0.5, 2.0)),
            Matrix4::from_translation(Vector3::new( 2.0, 0.5, 1.8)),
            Matrix4::from_translation(Vector3::new( 2.0, 0.5, 3.3)),
        ];

        Windows {
            shader_program: shader_program,
            vao: vao,
            vbo: vbo,
            model_matrices: model_matrices,
            texture_id: load_texture(&format!("{}/texture_window.png", src_folder), false, gl::CLAMP_TO_EDGE),
        }
    }

    fn init_opengl(src_folder: &str) -> (Shader, GLuint, GLuint) {

        let vertex_shader_path = &format!("{}/translucent_vertex_shader.glsl", src_folder);
        let fragment_shader_path = &format!("{}/translucent_fragment_shader.glsl", src_folder);

        unsafe {
            let shader_program = Shader::new(vertex_shader_path, fragment_shader_path);

        let vertices: [f32; 30] = [
            // positions      // texture Coords (swapped y coordinates because texture is flipped upside down)
            0.0,  0.5,  0.0,  0.0,  0.0,
            0.0, -0.5,  0.0,  0.0,  1.0,
            1.0, -0.5,  0.0,  1.0,  1.0,

            0.0,  0.5,  0.0,  0.0,  0.0,
            1.0, -0.5,  0.0,  1.0,  1.0,
            1.0,  0.5,  0.0,  1.0,  0.0
        ];
            let (mut vao, mut vbo) = (0, 0);
            gl::GenVertexArrays(1, &mut vao);
            gl::GenBuffers(1, &mut vbo);

            gl::BindVertexArray(vao);

            gl::BindBuffer(gl::ARRAY_BUFFER, vbo);
            gl::BufferData(gl::ARRAY_BUFFER,
                           (vertices.len() * mem::size_of::<GLfloat>()) as GLsizeiptr,
                           &vertices[0] as *const f32 as *const c_void,
                           gl::STATIC_DRAW);

            let stride = 5 * mem::size_of::<GLfloat>() as GLsizei;

            // positions
            gl::VertexAttribPointer(0, 3, gl::FLOAT, gl::FALSE, stride, ptr::null());
            gl::EnableVertexAttribArray(0);

            // texture coordinates
            let texture_coords_offset = (3 * mem::size_of::<GLfloat>()) as *const c_void;
            gl::VertexAttribPointer(1, 2, gl::FLOAT, gl::FALSE, stride, texture_coords_offset);
            gl::EnableVertexAttribArray(1);

            gl::BindBuffer(gl::ARRAY_BUFFER, 0);
            gl::BindVertexArray(0);

            (shader_program, vao, vbo)
        }
    }
}

impl SceneElement for Windows {

    fn render_frame(
        &self,
        _t: f32,
        _lights: &Vec<Light>,
        view_matrix: &Matrix4<f32>,
        projection_matrix: &Matrix4<f32>,
    ) {
        unsafe {
            gl::Enable(gl::BLEND);
            gl::BlendFunc(gl::SRC_ALPHA, gl::ONE_MINUS_SRC_ALPHA);

            self.shader_program.use_program();
            self.shader_program.set_mat4fv(c_str!("viewMatrix"), view_matrix);
            self.shader_program.set_mat4fv(c_str!("projectionMatrix"), projection_matrix);

            gl::ActiveTexture(gl::TEXTURE0);
            gl::BindTexture(gl::TEXTURE_2D, self.texture_id);
            gl::BindVertexArray(self.vao);

            // order windows by z position of their center point in view space to allow correct
            // rendering of translucent objects with depth testing turned on
            let mut reverse_z_ordered_model_matrices = self.model_matrices.clone();
            reverse_z_ordered_model_matrices.sort_unstable_by(|l_model_matrix, r_model_matrix| {
                let position: Vector4<f32> = Vector4::new(0.0, 0.0, 0.0, 1.0);
                let l_position = view_matrix * l_model_matrix * position;
                let r_position = view_matrix * r_model_matrix * position;
                l_position.z.partial_cmp(&r_position.z).expect("found NaN when computing z positions of windows")
            });
            let reverse_z_ordered_model_matrices = reverse_z_ordered_model_matrices;

            for model_matrix in reverse_z_ordered_model_matrices.iter() {
                self.shader_program.set_mat4fv(c_str!("modelMatrix"), &model_matrix);
                gl::DrawArrays(gl::TRIANGLES, 0, 6);
            }
        }
    }
}

impl Drop for Windows {
    fn drop(&mut self) {
        unsafe {
            gl::DeleteVertexArrays(1, &self.vao);
            gl::DeleteBuffers(1, &self.vbo);
        }
    }
}
