#version 330 core

uniform sampler2D uTexture;

in vec2 texCoord;

out vec4 fragColor;

void main() {
	vec4 textureColor = texture(uTexture, texCoord);
	if(textureColor.a < 0.1) {
		discard;
	}
	fragColor = textureColor;
}
